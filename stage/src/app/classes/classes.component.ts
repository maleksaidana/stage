import { Component, OnInit } from '@angular/core';
import { faUsers} from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute } from '@angular/router';
import {AuthService } from './../shared/auth.service';
import {AdminService } from './../shared/admin.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.scss']
})
export class ClassesComponent implements OnInit {
  classListIcon=faUsers;

  private sub: any;
  auth:AuthService;
  Semester:any;

  constructor( public rout: Router,auth:AuthService,private route: ActivatedRoute, public adminservice: AdminService ) {this.auth=auth }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
     
      this.semester(params['id_semester']);
   });
  }


  semester(_id){
    var semester:any;
    this.adminservice.get_semester({"_id":_id}).then(data =>{
      semester=data;
       // //console.log('postcomment =', comm_res);
        if(semester)
        {
          
       
          this.Semester=semester;
          this.Semester=this.Semester.u[0];
          
          //console.log("SEMESTTTTEER",this.Semester);
        }
    });



  }


  groups(id)
  {
    this.rout.navigate(['groups/'+id]);
  }

}
