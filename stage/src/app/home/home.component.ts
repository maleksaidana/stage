import { Component, OnInit } from '@angular/core';
import { faFile,faArrowCircleUp,faNewspaper,faPaperclip,faImages,faPaperPlane,faEllipsisH,faEyeSlash,faMinusCircle,faClock,faThumbsUp,faComments,faReply,faPlus, faTrash, faCheck } from '@fortawesome/free-solid-svg-icons';
import {InfService } from './../shared/inf.service';
import { ActivatedRoute,Router } from '@angular/router';

import * as $ from 'jquery'
import {AuthService } from './../shared/auth.service';
import {HostListener }  from '@angular/core';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  backToTopIcon= faArrowCircleUp;
  newsfeedIcon= faNewspaper;
  attachementIcon= faPaperclip;
  mediaIcon= faImages;
  sendIcon= faPaperPlane;
  optionsIcon= faEllipsisH;
  hideIcon= faEyeSlash;
  reportIcon= faMinusCircle;
  timeIcon= faClock;
  likeIcon= faThumbsUp;
  commentIcon= faComments;
  replyIcon= faReply;
  moreIcon= faPlus;
  fileIcon=faFile;
  deleteIcon=faTrash
  confirmIcon=faCheck;

  options:Array<any>=[
    {"selected":true,"key":0,"value":"none"},
    {"selected":false,"key":1,"value":"I am offering"},
    {"selected":false,"key":2,"value":"I am searching"}
]
  uploadedFiles: Array < File >=[];
  url;
  msg = "";
  todayNumber: number;
  URLs:Array<String> = [];
  files:Array<{size: String, name: string}> = [];

  posts:any;
  post_file=this.infoservice.authService.endpoint+"/uploads/file_posts/";
  post_images=this.infoservice.authService.endpoint+"/uploads/images_posts/";
  profile_pic_target=this.infoservice.authService.endpoint+"/uploads/profile_images/";
  profile_pic=this.infoservice.authService.endpoint+"/uploads/profile_images/"+this.authService.get_id()+".jpg";

post_type="3";

  constructor(public infoservice: InfService,public route: Router,public authService: AuthService) { }

  ngOnInit(): void {
    
    this.getposts();
  }
  uploadImageClick(){
    document.getElementById("uploadimagebtn").click();
  }
  uploadFileClick(){
    document.getElementById("uploadfilebtn").click();
  }
  uploadimage(event) {


    if(!event.target.files[0] || event.target.files[0].length >1) {
      this.msg = 'You must select one image at a time';
      window.alert(this.msg);
			return; 
		}
		
		var mimeType = event.target.files[0].type;
		
		if (mimeType.match(/image\/*/) == null) {
      this.msg = "Only images are supported";
      window.alert(this.msg);
			return;
		}
    this.uploadedFiles.push(event.target.files[0]);
  
		var reader = new FileReader();
		reader.readAsDataURL(event.target.files[0]);
		
		reader.onload = (_event) => {
			this.msg = "";
      this.url = reader.result; 
      this.URLs.push(this.url);
    }
  }
  send(){
    var content = $('#NewsTextarea').val();
    var type = $('#post_type').val();
    if(content=="" && this.uploadedFiles.length==0)
    {
      //console.log(this.uploadedFiles)
      this.msg="You must select a file/image OR enter something";
      return;
    }
    this.todayNumber =Date.now();
    var year=new Date().getFullYear();
    var month=new Date().getMonth()+1;
    var day=new Date().getDate();
    var hours=new Date().getHours();
    var minutes=new Date().getMinutes();
    var seconds=new Date().getSeconds();
    var today = year+"-"+month+"-"+day+" "+hours+":"+minutes+":"+seconds;
    this.infoservice.upload_posts(this.uploadedFiles,content,this.todayNumber,'0',type).then(dataPost =>{
      var post:any;
      post=dataPost
      
      //console.log('hahahahah', post);
      post.userinfo={"firstname":this.authService.get_firstname(),"_id":this.authService.get_id(),"lastname":this.authService.get_lastname()}
      post.is_liked=0;
      post.totalComments=0;
      post.totalLikes=0;
      post.comment=[];
      this.posts.unshift(post);
      this.getImagesFromString(post.image,post._id);
      this.getFilesFromString(post.file,post._id); 
       $('#NewsTextarea').val("");
       this.files=[];
       this.URLs=[];
       this.uploadedFiles=[];

    });

    
  }
  uploadfile(event)
  {
    if(!event.target.files[0] || event.target.files[0].length >1) {
      this.msg = 'You must select one file at a time';
      window.alert(this.msg);
			return;
		}
		
		var mimeType = event.target.files[0].type;
		
		if (mimeType.match(/image\/*/) != null) {
      this.msg = "Only files are supported";
      window.alert(this.msg);
			return;
    }
    
    this.uploadedFiles.push(event.target.files[0]);
    //console.log(event.target.files[0])
      this.msg = "";
      var bytes="Kb";
      var size=Math.floor(event.target.files[0].size/1000);
      if(event.target.files[0].size<1000)
      {
        size=event.target.files[0].size;
        bytes="b";
      }
      
      if(size>999)
      {bytes="Mb"}
      this.files.push({size: (size.toString()+" "+bytes), name: (event.target.files[0].name)});
    
  }

  @HostListener("window:scroll", ["$event"])
  onWindowScroll() {
  let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
  let max = document.documentElement.scrollHeight;  
  
   if(pos >= max-200)   {
    
    $('#pageLoad').removeClass('d-none');
    if(this.posts.skip>this.posts.totalPosts){
       
       $('#pageLoad').addClass('d-none');
    }
    this.posts.skip= this.posts.skip+3; 
    this.appendPosts(this.posts.skip);

   }
  } 

  getposts(){

    this.infoservice.getPosts("",0,"0",this.post_type).then(dataPosts =>{
      this.posts=dataPosts;
      this.posts=this.posts.post;
      this.getPostsCount();
      this.getpostinfos();
      this.posts.skip=0;
      //console.log('asba =', this.posts);
  });

  }

  
getPostsCount()
{
   var count_res:any;
 this.infoservice.get_posts_count("","0",this.post_type).then(dataCount =>{
   count_res=dataCount;
    // //console.log('postcomment =', comm_res);
     if(count_res)
     {
       
    
       this.posts.totalPosts=count_res.pos;
       ////console.log(this.posts)
     }
 });
}


getpostinfos()
{
  for(let post of this.posts)
  {
    this.getComments(post._id);
    this.getCommentsCount(post._id);
    this.getLikesCount(post._id);
    this.getIsLiked(post._id);
    this.getImagesFromString(post.image,post._id);
    this.getFilesFromString(post.file,post._id);
    post.userinfo.profile_pic=this.infoservice.authService.endpoint+"/uploads/profile_images/"+post.userinfo._id+".jpg";
  }
}


getComments(idPost)
{
   var comm_res:any;
 this.infoservice.get_comments(idPost,0).then(dataComment =>{
     comm_res=dataComment;
    // //console.log('postcomment =', comm_res);
     if(comm_res)
     {
       var post:any=this.posts.find(x => x._id === idPost);

       post.comment=comm_res.comment;
       post.comment.skip=5;
       ////console.log(this.posts)
      /* for(let c of comm_res.comment){
        post.comment.push(c);
      }*/
     }
 });
}


getCommentsCount(idPost)
{
   var comm_count_res:any;
 this.infoservice.get_comments_count(idPost).then(dataCommentCount =>{
  comm_count_res=dataCommentCount;
    // //console.log('postcomment =', comm_res);
     if(comm_count_res)
     {
       var post:any=this.posts.find(x => x._id === idPost);
    
       post.totalComments=comm_count_res.comm;
       ////console.log(this.posts)
     }
 });
}


getLikesCount(idPost)
{
   var like_res:any;
 this.infoservice.get_likes_count(idPost).then(dataLike =>{
  like_res=dataLike;
    // //console.log('postcomment =', comm_res);
     if(like_res)
     {
       var post:any=this.posts.find(x => x._id === idPost);

       post.totalLikes=like_res.like;
       ////console.log(this.posts)
     }
 });
}

getIsLiked(idPost)
{
  var like_res:any;
 this.infoservice.get_is_liked(idPost).then(dataLike =>{
  like_res=dataLike;
    // //console.log('postcomment =', comm_res);
     if(like_res)
     {
       var post:any=this.posts.find(x => x._id === idPost);

       post.is_liked=like_res.liked;
       ////console.log(this.posts)
     }
 });
}

getImagesFromString(i,idPost)
{
  
 var files_string=""+i;
 if(files_string=="")
{return;}

 var splitted=files_string.split(";");

var images:Array<{iterator: String, url: string}> = [];
 for(var j=0;j<splitted.length-1;j++)
 {
   images.push({url:this.post_images+splitted[j].toString(),iterator:j.toString()});
 }
 var post:any=this.posts.find(x => x._id === idPost);

  post.images=images;
}

getFilesFromString(f,idPost)
{
 var files_string=""+f;
 if(files_string=="")
 {return;}
 var splitted=files_string.split(";");
 
 var files:Array<{url: String, name: string}> = [];
 for(var i=0;i<splitted.length-1;i++)
 {
  files.push({name: splitted[i].toString(),url:this.post_file+splitted[i].toString()});
 }
 var post:any=this.posts.find(x => x._id === idPost);

 post.files=files;
}

checkIdUser(idUser):boolean
{

  return (idUser==this.infoservice.authService.get_id())?true:false;
}
deletePost(idPost){
  var result = confirm("Are you sure?");
  if (!result) {
      return;
  }
  var comment_res:any;
  this.infoservice.delete_post(idPost).then(dataPost =>{
    comment_res=dataPost;
      ////console.log('postcomment =', like_res);
      if(comment_res)
      {
          var post:any=this.posts.find(x => x._id === idPost);
          var posts:any=this.posts;
          ////console.log("finall1",this.posts)
          var p = posts.indexOf(post);
          posts.splice(p,1);
          this.showConfirmModal()
          ////console.log("finall2",this.posts);
  
      }
  });
}

showConfirmModal()
{
  document.getElementById("openConfirmModalButton").click();
  setTimeout(function() {$('#confirmmodalbtn').click();}, 1000);

}
upperCase(s)
 {
  var s_string:String=String(s);
  return s_string.charAt(0).toUpperCase()+s_string.slice(1)
 }

 getDate(d)
  {
    let newDate = new Date(d);
    return this.timeSince(newDate);

  }

  
  timeSince(date) {
    var seconds = Math.floor((Date.now() - date) / 1000);
    
    var interval = seconds / 31536000;
  
    if (interval > 1) {
      return Math.floor(interval) + " years ago";
    }
    interval = seconds / 2592000;
    if (interval > 1) {
      return Math.floor(interval) + " months ago";
    }
    interval = seconds / 86400;
    if (interval > 1) {
      return Math.floor(interval) + " days ago";
    }
    interval = seconds / 3600;
    if (interval > 1) {
      return Math.floor(interval) + " hours ago";
    }
    interval = seconds / 60;
    if (interval > 1) {
      return Math.floor(interval) + " minutes ago";
    }
    return Math.floor(seconds) + " seconds ago";
  }


  deleteComment(idComment,idPost){
    var result = confirm("Are you sure?");
    if (!result) {
        return;
    }
    var comment_res:any;
    this.infoservice.delete_comment(idComment).then(dataComment =>{
      comment_res=dataComment;
        ////console.log('postcomment =', like_res);
        if(comment_res)
        {
          var post:any=this.posts.find(x => x._id === idPost);
          var comments:any=post.comment
          var comment:any=comments.find(x=>x._id ===idComment)
          //console.log("finall",comment)
          var c = comments.indexOf(comment);
          comments.splice(c,1);
          var tot=parseInt(($('#tot'+idPost).html()).toString()) -1;
          $('#tot'+idPost).html(""+tot);
          this.showConfirmModal()
          //post.totalComments=post.totalComments-1;
        }
    });
  }
  
  deleteReply(idReply,idComment,idPost){
    var result = confirm("Are you sure?");
    if (!result) {
        return;
    }
    
    var reply_res:any;
    this.infoservice.delete_reply(idReply).then(dataReply =>{
      reply_res=dataReply;
        ////console.log('postcomment =', like_res);
        if(reply_res)
        {
          var post:any=this.posts.find(x => x._id === idPost);
          var comments:any=post.comment
          var comment:any=comments.find(x=>x._id ===idComment)
          var replies:any=comment.reply
          var reply:any=replies.find(x=>x._id ===idReply)
          //console.log("finall",comment)
          var r = replies.indexOf(reply);
          replies.splice(r,1);
          this.showConfirmModal()
        // comment.totalReplies=comment.totalReplies-1;
        }
    });
  }
  
  
  
   postComment(idPost)
   {
     var content = $('#textarea_'+idPost).val();
      if(content=="")
      {
        window.alert("you must enter something");
        return;
      }
      var comm_res:any;
    this.infoservice.post_comment(idPost,content).then(dataComment =>{
        comm_res=dataComment;
       // //console.log('postcomment =', comm_res);
        if(comm_res)
        {
          var post:any=this.posts.find(x => x._id === idPost);
          ////console.log("finall",post)
          comm_res.firstname=this.infoservice.authService.get_firstname();
          comm_res.lastname=this.upperCase(this.infoservice.authService.get_lastname());
          post.comment.push(comm_res);
          ////console.log(this.posts)
          //post.totalComments=post.totalComments+1;
          var tot=parseInt(($('#tot'+idPost).html()).toString()) +1;
          $('#tot'+idPost).html(tot+"");
          $('#collapseCommentsToggle_'+idPost).click();
          $('#textarea_'+idPost).val("");
        }
    });
   }
  
  
   postReply(idComment,idPost)
   {
     var content = $('#textarea_'+idComment).val();
      if(content=="")
      {
        window.alert("you must enter something");
        return;
      }
      var reply_res:any;
    this.infoservice.post_reply(idComment,idPost,content).then(dataReply =>{
        reply_res=dataReply;
       // //console.log('postcomment =', comm_res);
        if(reply_res)
        {
          var post:any=this.posts.find(x => x._id === idPost);
          var comments:any=post.comment;
          
          var comment:any=comments.find(x => x._id === idComment);
  
          reply_res.firstname=this.infoservice.authService.get_firstname();
          reply_res.lastname=this.upperCase(this.infoservice.authService.get_lastname());
          comment.reply.push(reply_res);
          $('#textarea_'+idComment).val("");
        }
    });
   }

   getReplies(idPost,idComment)
{
  ////console.log('comm',idComment)
   var reply_res:any;
   this.infoservice.get_replies(idComment,0).then(dataReplies =>{
    reply_res=dataReplies;
    // //console.log('postcomment =', comm_res);
     if(reply_res)
     {
      var post:any=this.posts.find(x => x._id === idPost);
       var comments:any=post.comment;
       
       var comment:any=comments.find(x => x._id === idComment);
       ////console.log('rep',reply_res)
       comment.reply=reply_res.reply;
       comment.skip=3;
       this.getRepliesCount(idComment,post._id);
       ////console.log(this.posts)
     }
 });
}

getRepliesCount(idComment,idPost)
{
   var comm_count_res:any;
 this.infoservice.get_replies_count(idComment).then(dataCommentCount =>{
  comm_count_res=dataCommentCount;
    // //console.log('postcomment =', comm_res);
     if(comm_count_res)
     {
       var post:any=this.posts.find(x => x._id === idPost);
    

       var post:any=this.posts.find(x => x._id === idPost);
       var comments:any=post.comment;
       
       var comment:any=comments.find(x => x._id === idComment);
       ////console.log('rep',reply_res)

       comment.totalReplies=comm_count_res.rep;
       //console.log("hahaha",this.posts)
     }
 });
}
  
  
   like(idPost)
   {
  
      var like_res:any;
    this.infoservice.post_like(idPost).then(dataLike =>{
      like_res=dataLike;
        ////console.log('postcomment =', like_res);
        if(like_res)
        {
          var post:any=this.posts.find(x => x._id === idPost);
          ////console.log("finall",post)
          post.is_liked="1";
          post.totalLikes=post.totalLikes+1;
        }
    });
   }
   unlike(idPost)
   {
      var like_res:any;
    this.infoservice.post_unlike(idPost).then(dataLike =>{
      like_res=dataLike;
        ////console.log('postcomment =', like_res);
        if(like_res)
        {
          var post:any=this.posts.find(x => x._id === idPost);
          ////console.log("finall",post)
          post.is_liked="0";
          post.totalLikes=post.totalLikes-1;
        }
    });
   }
   appendComments(idPost,skip)
   {
      var comm_res:any;
    this.infoservice.get_comments(idPost,skip).then(dataComment =>{
        comm_res=dataComment;
       // //console.log('postcomment =', comm_res);
        if(comm_res)
        {
          var post:any=this.posts.find(x => x._id === idPost);
        for(let c of comm_res.comment){
           post.comment.push(c);
           
         }
         post.comment.skip= post.comment.skip+5;
        }
    });
   }
   
   appendPosts(skip)
   {
    
     var post:any;
     if(this.posts.skip<this.posts.totalPosts){
     
   
   
    this.infoservice.getPosts("",skip,"0",this.post_type).then(dataPosts =>{
   
     post=dataPosts;
     for(let p of post.post){
       this.posts.push(p);
     }
     $('#pageLoad').addClass('d-none');
     this.getPostsCount();
     this.getpostinfos();
    // this.posts.skip= this.posts.skip+3;
     //console.log('asba2 =', this.posts);
   });
   
   }
   
   }


   appendReplies(idPost,idComment,skip)
{
  ////console.log('comm',idComment)
   var reply_res:any;
   this.infoservice.get_replies(idComment,skip).then(dataReplies =>{
    reply_res=dataReplies;
    // //console.log('postcomment =', comm_res);
     if(reply_res)
     {
      var post:any=this.posts.find(x => x._id === idPost);
       var comments:any=post.comment;
       
       var comment:any=comments.find(x => x._id === idComment);
       ////console.log('rep',reply_res)

       for(let c of reply_res.reply){
        comment.reply.push(c);
        
      }
      comment.skip= comment.skip+3;
      // comment.reply=reply_res.reply;
     }
 });
}
visitProfile(idUser)
{
  this.route.navigate(['profile/'+idUser]);
}


onChange(type) {
  
  this.post_type=type.toString().trim();
  this.getposts();

}


}

