import { Component, OnInit } from '@angular/core';
import { faInbox, faSearch, faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import { HttpClient } from '@angular/common/http';
import {AuthService } from './../shared/auth.service';
import {InfService } from './../shared/inf.service';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {
  inboxIcon= faInbox;
  searchIcon = faSearch;
  sendIcon= faPaperPlane;
  auth:AuthService
  uploadedFiles: Array < File > ;
  infoservice:InfService
 
  constructor(private http: HttpClient,  auth:AuthService,  infoservice:InfService) {  
    this.auth=auth;
    this.infoservice=infoservice;
  }

  ngOnInit(): void {
    this.classes();
  }


  fileChange(element) {
    this.uploadedFiles = element.target.files;
}

upload() {
    let formData = new FormData();
    var str;
    var splitted;
    for (var i = 0; i < this.uploadedFiles.length; i++) {
     str=  (this.uploadedFiles[i].name)
     splitted=str.split(['.'],2);
     splitted="salah."+splitted[1];
     //console.log(splitted);
        formData.append("uploads[]", this.uploadedFiles[i],splitted );
    }
    this.http.post('http://localhost:3000/upload', formData)
        .subscribe((response) => {
            //console.log('response received is ', response);
        })
}

 
classes(){
  this.infoservice.getClasses().then(dataClasses =>{
  
    //console.log('asba =', dataClasses);
});
}

}
 