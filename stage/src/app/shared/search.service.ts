import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import {  HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable()
export class SearchService {
  endpoint: string = this.authService.endpoint;
headers = new HttpHeaders().set('Content-Type', 'application/json');
constructor(private _http: Http,public authService: AuthService) { }
search(queryString: string) {
      return this._http.post(this.endpoint+"/search",{"search":queryString});
  }
}