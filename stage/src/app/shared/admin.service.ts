import { Injectable } from '@angular/core';

import { AuthService } from './../shared/auth.service';
import {  HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AdminService {
  endpoint: string = this.authService.endpoint;
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  uploadedFiles: Array < File > ;
  countCom:any;

  constructor(public http: HttpClient,public authService: AuthService, private httpClient: HttpClient, public router: Router) { }
  public dataR:Array<any>;
  get_posts_count() {
    return new Promise((resolve, reject) => {
     this.httpClient.get<any>(`${this.endpoint}/countAllPosts`).subscribe(data => {
         let dataposts = data;
         ////console.log(dataLike)
         resolve(dataposts.count); 
       });
   });
  }
  get_users_count() {
    return new Promise((resolve, reject) => {
     this.httpClient.get<any>(`${this.endpoint}/countAllUsers`).subscribe(data => {
         let datausers = data;
         ////console.log(dataLike)
         resolve(datausers.count); 
       });
   });
  }
  getUserInfo(da) :any {
    // return a new promise, which you resolve once you get the data
    return new Promise((resolve, reject) => {
      this.httpClient.post<any>(`${this.endpoint}/infosDA`,{'da':da}).subscribe(data => {
          let dataR = data;
          // resolve promise with username
          resolve(dataR);
        });
    });
  }
  getUser(_id) :any {
    // return a new promise, which you resolve once you get the data
    return new Promise((resolve, reject) => {
      this.httpClient.post<any>(`${this.endpoint}/infos`,{'_id':_id}).subscribe(data => {
          let dataR = data;
          // resolve promise with username
          resolve(dataR);
        });
    });
  }
  getClassInfo(name):any{
    return new Promise((resolve, reject) => {
      this.httpClient.post<any>(`${this.endpoint}/classGroups`,{'name':name}).subscribe(data => {
          let dataClass = data;
          // resolve promise with username
          resolve(dataClass);
        });
    });
  }
  add_class(name,description,users,semester)
  {

   var us:any=[]; 
  for(let user of users)
  {
    var  u={"member":user._id}; 
      us.push(u);
  }
    var classe={"name":name,"description":description,"groups":[{"group":us}]};
    var send={"c":classe,semester}
    return new Promise((resolve, reject) => {
      this.httpClient.post<any>(`${this.endpoint}/newClass`,send).subscribe(data => {
          let dataR = data;
          // resolve promise with username
          //console.log("hahahah",dataR)
          resolve(dataR);
        });
    });
  }
  add_semester(name,description)
  {

    var s={"name":name,"description":description};
    return new Promise((resolve, reject) => {
      this.httpClient.post<any>(`${this.endpoint}/newSemester`,s).subscribe(data => {
          let dataR = data;
          // resolve promise with username
          resolve(dataR);
        });
    });
  }
  add_group(users,idClass)
  {
    var us:any=[]; 
    for(let user of users)
    {
      var  u={"member":user._id}; 
        us.push(u);
    }
    var group={"group":us,"_id_class":idClass}
   
      return new Promise((resolve, reject) => {
        this.httpClient.post<any>(`${this.endpoint}/appendClass`,{"grouptoadd":group}).subscribe(data => {
            let dataR = data;
            // resolve promise with username
            resolve(dataR);
          });
      });
  }
  add_user(firstname,lastname,birthdate,da,program,password,role)
  {
    

    if(role=="student"){
   var  s={"da":da,"firstname":firstname,"lastname":lastname,"birthdate":birthdate,"prog":program,"password":password};
   return new Promise((resolve, reject) => {
      this.httpClient.post<any>(`${this.endpoint}/newStudent`,s).subscribe(data => {
          let dataR = data;
      
          resolve(dataR);
        });
    });

    }
    else if(role=="teacher"){
       var t={"da":da,"firstname":firstname,"lastname":lastname,"birthdate":birthdate,"speciality":program,"password":password};

      return new Promise((resolve, reject) => {
        this.httpClient.post<any>(`${this.endpoint}/newTeacher`,t).subscribe(data => {
            let dataR = data;
            // resolve promise with username
            resolve(dataR);
          });
      });

    }
  }
  remove_user(id)
  { 
    return new Promise((resolve, reject) => {
        this.httpClient.post<any>(`${this.endpoint}/removeUser`,{"_id":id}).subscribe(data => {
            let dataR = data;
            // resolve promise with username
            resolve(dataR);
          });
      });
    
  }
  edit_user(id,role,firstname,lastname,birthdate,da,program)
  {
    if(role=="teacher")
    {
      var teacher:any={"_id":id,"firstname":firstname,"lastname":lastname,"birthdate":birthdate,"fonction":program}
      return new Promise((resolve, reject) => {
        this.httpClient.post<any>(`${this.endpoint}/editTeacher`,teacher).subscribe(data => {
            let dataR = data;
            // resolve promise with username
            resolve(dataR);
          });
      });
    }
    else{
      var student:any={"_id":id,"firstname":firstname,"lastname":lastname,"birthdate":birthdate,"prog":program}
      return new Promise((resolve, reject) => {
        this.httpClient.post<any>(`${this.endpoint}/editStudent`,student).subscribe(data => {
            let dataR = data;
            // resolve promise with username
            resolve(dataR);
          });
      });
    }
    
  }
  edit_class(description,semester,classe)
  {
    
    var gs:any=[]; 
    for(let group of classe.groups)
    {
      var g:any=[]; 
      for(let student of group.group.student)
      {
         var  u={"member":student._id}; 
        g.push(u)
      }
      gs.push({"group":g,"_id":group._id});
    }
    
      var classe2={"name":classe.name,"_id":classe._id,"description":description,"groups":gs};
      var send={"c":classe2,"oldSemester":classe.semester[0]._id,"newSemester":semester}
    //  //console.log(send)
  //    //console.log(g)

      return new Promise((resolve, reject) => {
        this.httpClient.post<any>(`${this.endpoint}/editClass`,send).subscribe(data => {
            let dataR = data;
            //console.log(dataR)
            resolve(dataR);
          });
      });
  }
  get_sessions() {
    //this.uploadedFiles = element.target.files;
     // //console.log(formData)
   
     return new Promise((resolve, reject) => {
      this.httpClient.post<any>(`${this.endpoint}/sessions`,"").subscribe(data => {
          let sessions = data;
          ////console.log(dataComment)
          resolve(sessions);
        });
    });
  }


  get_semester(_id) {
    //this.uploadedFiles = element.target.files;
     // //console.log(formData)
   
     return new Promise((resolve, reject) => {
      this.httpClient.post<any>(`${this.endpoint}/semester`,_id).subscribe(data => {
          let semester = data;
          ////console.log(dataComment)
          resolve(semester);
        });
    });
  }


  get_classe(_id) {
    //this.uploadedFiles = element.target.files;
     // //console.log(formData)
   
     return new Promise((resolve, reject) => {
      this.httpClient.post<any>(`${this.endpoint}/groups`,_id).subscribe(data => {
          let classe = data;
          ////console.log(dataComment)
          resolve(classe);
        });
    });
  }









  upload(element,idClass) {
    this.uploadedFiles = element.target.files;
      let formData = new FormData();
      var str;
      var splitted;
      for (var i = 0; i < this.uploadedFiles.length; i++) {
       str=  (this.uploadedFiles[i].name)
       splitted=str.split(['.'],2);
       splitted=idClass+".jpg";
       ////console.log(splitted);
          formData.append("uploads[]", this.uploadedFiles[i],splitted );
      }
      this.http.post(this.endpoint+"/uploadClass", formData)
          .subscribe((response) => {
              //console.log('response received is ', response);
              //window.location.reload();
          })
  }
}
