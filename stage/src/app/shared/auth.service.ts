import { Injectable } from '@angular/core';
import { User } from './user';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  endpoint: string = 'http://localhost:3000'; 
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  currentUser = {};

  constructor(
    private http: HttpClient,
    public router: Router
  ) {
  }

 
 

  // Sign-in
  signIn(user: User, path) {
  /*  return this.http.post<any>(`${this.endpoint}${path}`, user) 
      .subscribe((res: any) => {
        localStorage.setItem('access_token', res.accessToken)
        this.router.navigate(['home']); 
        this.getbooks();*/
      
     //   //console.log(this.getToken()); 
       /* this.getUserProfile(res.accessToken).subscribe((res) => {
          this.currentUser = res;
          //console.log(this.getToken()); 
          //this.router.navigate(['home/' + res.msg._id]);
          this.router.navigate(['home']);
        })*/
    /*  },
      (error)=>{
        //console.log("DA number or Password incorrect");
       
      })*/

      return new Promise((resolve, reject) => {
        this.http.post<any>(`${this.endpoint}${path}`, user) 
        .subscribe((data:any) => {
          localStorage.setItem('access_token', data.accessToken)
          this.router.navigate(['home']); 
            resolve(data);
          },
          (error)=>{
          
            resolve("Username Or Password Incorrect");
           
          })
      });
     
  }

  getToken() {
    return localStorage.getItem('access_token');
  }  

  getRole(){
    const decoded = jwt_decode(this.getToken());
    return decoded.role;
  }

  get_id(){
    const decoded = jwt_decode(this.getToken());
    return decoded._id;
  }

  get_firstname(){
    const decoded = jwt_decode(this.getToken());
    return decoded.firstname;
  }

  get_lastname(){
    const decoded = jwt_decode(this.getToken());
    return decoded.lastname;
  }

  get_prog(){
    const decoded = jwt_decode(this.getToken());
    return decoded.prog;
    
  }
  get_speciality(){
    const decoded = jwt_decode(this.getToken());
    return decoded.speciality;
    
  }
  get_fonction(){
    const decoded = jwt_decode(this.getToken());
    return decoded.fonction;
    
  }


  get isTeacher():boolean{
  
    if(this.getRole()==="teacher")
    return true;
    else
    return false;
  }

  get isAdmin():boolean{
  
    if(this.getRole()==="admin") 
    return true;
    else
    return false;
  }

  get isStudent():boolean{
  
    if(this.getRole()==="student")
    return true;
    else
    return false;
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('access_token');
  //  return (authToken !== null) ? true : false;

    if(authToken!=null && !this.isTokenExpired(authToken))
    return true;
    else
    return false;
  }

  doLogout() {
    let removeToken = localStorage.removeItem('access_token');
    if (removeToken == null) {
      this.router.navigate(['']);
    }
  }

  // User profile
  getUserProfile(id): Observable<any> {
    let api = `${this.endpoint}/home/${id}`;
    return this.http.get(api, { headers: this.headers }).pipe(
      map((res: Response) => {
        return res || {}
      }),
      catchError(this.handleError)
    )
  }

  getbooks(){

    return this.http.get<any>(`${this.endpoint}/books`)
    .subscribe((res: any) => {
       
      //console.log(res); 
 
    })
 
      catchError(this.handleError)
      
  }

  // Error 
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }


  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);

    if (decoded.exp === undefined) return null;

    const date = new Date(0); 
    date.setUTCSeconds(decoded.exp);
    return date;
  }


  isTokenExpired(token?: string): boolean {
    if(!token) token = this.getToken();
    if(!token) return true;

    const date = this.getTokenExpirationDate(token);
    if(date === undefined) return false;
    return !(date.valueOf() > new Date().valueOf());
  }

}
