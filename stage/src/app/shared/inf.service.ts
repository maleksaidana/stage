import { Injectable } from '@angular/core';
import { AuthService } from './../shared/auth.service';
import {  HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { User } from './user';
import { switchMap } from 'rxjs/operators'


@Injectable({
  providedIn: 'root'
})
export class InfService {

  endpoint: string = this.authService.endpoint;
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  uploadedFiles: Array < File > ;
  countCom:any;
  constructor(public http: HttpClient,public authService: AuthService, private httpClient: HttpClient, public router: Router) { }



  public dataR:Array<any>;

  getUserInfo(_id) :any {
    // return a new promise, which you resolve once you get the data
    return new Promise((resolve, reject) => {
      this.httpClient.post<any>(`${this.endpoint}/infos`,{'_id':_id}).subscribe(data => {
          let dataR = data;
          // resolve promise with username
          resolve(dataR);
        });
    });
  }

  public dataPosts:Array<any>;

  getPosts(_id,skip,classroom,type) {

    return new Promise((resolve, reject) => {
      this.httpClient.post<any>(`${this.endpoint}/posts`,{"_id":_id,"skip":skip,"classroom":classroom,"type":type}).subscribe(data => {
          let dataPosts = data;
          resolve(dataPosts);
        });
    });


  }


  getClasses() { 

    return new Promise((resolve, reject) => {
      this.httpClient.post<any>(`${this.endpoint}/Classes`,"").subscribe(data => {
          let dataClasses = data;
          resolve(dataClasses);
        });
    });


  }

  get_posts_count(id,classroom,type) {
    return new Promise((resolve, reject) => {
     this.httpClient.post<any>(`${this.endpoint}/countPosts`,{"_id_user":id,"classroom":classroom,"type":type}).subscribe(data => {
         let dataposts = data;
         ////console.log(dataLike)
         resolve(dataposts); 
       });
   });
  }

    getCountComments(_id):Promise<any> {
      return this.httpClient.post<any>(`${this.endpoint}/countComments`,{"_id_post":_id}).toPromise();
 
  }

    



  fileChange(element) {
    this.uploadedFiles = element.target.files;
}

upload(element) {
  this.uploadedFiles = element.target.files;
    let formData = new FormData();
    var str;
    var splitted;
    for (var i = 0; i < this.uploadedFiles.length; i++) {
     str=  (this.uploadedFiles[i].name)
     splitted=str.split(['.'],2);
     splitted=this.authService.get_id()+".jpg";
     ////console.log(splitted);
        formData.append("uploads[]", this.uploadedFiles[i],splitted );
    }
    this.http.post(this.endpoint+"/upload", formData)
        .subscribe((response) => {
            //console.log('response received is ', response);
            //window.location.reload();
        })
}
upload_posts(uploadedFile,content,filedate,classroom,type) {
  //this.uploadedFiles = element.target.files;
    let formData = new FormData();
    var str;
    var splitted;
    var images_string="";
    var files_string="";
    for (var i = 0; i < uploadedFile.length; i++) {
      var mimeType = uploadedFile[i].type;
		  str=  (uploadedFile[i].name)
      
     splitted=str.split(".",2);
     splitted=this.authService.get_id()+"_"+filedate+"_"+i+'.'+splitted[1];
     if (mimeType.match(/image\/*/) == null) {
      files_string+=splitted+";"
     }
     else{
      images_string+=splitted+";"
     }
        formData.append("uploads[]", uploadedFile[i],splitted );
    }
   // //console.log(formData)
    var post={"content":content,"classroom":classroom,  "image":images_string,"file":files_string,"type":type}
   
   
    formData.append("post", JSON.stringify(post) );
  
  

        return new Promise((resolve, reject) => { 
          this.httpClient.post<any>(`${this.endpoint}/newPost`,formData).subscribe(data => {
              let dataPost = data;
              ////console.log('response received is ', dataPost);
              resolve(dataPost);
            });
        });
}
public dataComment:Array<any>; 

post_comment(idPost,content) {
  //this.uploadedFiles = element.target.files;
   // //console.log(formData)
   var comment={"content":content,"_id_post":idPost}
   return new Promise((resolve, reject) => {
    this.httpClient.post<any>(`${this.endpoint}/newComment`,comment).subscribe(data => {
        let dataComment = data;
        ////console.log(dataComment)
        resolve(dataComment);
      });
  });
}


get_my_classes() {
  //this.uploadedFiles = element.target.files;
   // //console.log(formData)
 
   return new Promise((resolve, reject) => {
    this.httpClient.post<any>(`${this.endpoint}/MyClasses`,"").subscribe(data => {
        let classes = data;
        ////console.log(dataComment)
        resolve(classes);
      });
  });
}


verify_password(password)
{
  var verify={"password":password}
  return new Promise((resolve, reject) => {
   this.httpClient.post<any>(`${this.endpoint}/verifyPassword`,verify).subscribe(data => {
       let dataVerify = data;
       resolve(dataVerify); 
     });
 });
}
edit_profile(email,phone,password,newpassword,confirmnewpassword)
{
  var edit={"email":email,"phone":phone,"password":password,"newpassword":newpassword,"confirmnewpassword":confirmnewpassword}
  return new Promise((resolve, reject) => {
   this.httpClient.post<any>(`${this.endpoint}/editUser`,edit).subscribe(data => {
       let dataEdit = data;
       
       resolve(dataEdit); 
     });
 });
}
post_reply(idComment,idPost,content) {
  //this.uploadedFiles = element.target.files;
   // //console.log(formData)
   var reply={"_id_post":idPost,"content":content,"_id_comment":idComment}
   return new Promise((resolve, reject) => {
    this.httpClient.post<any>(`${this.endpoint}/newReply`,reply).subscribe(data => {
        let dataReply = data;
        ////console.log(dataComment)
        resolve(dataReply);
      });
  });
}
delete_post(idPost)
{
  return new Promise((resolve, reject) => {
    this.httpClient.post<any>(`${this.endpoint}/deletePost`,{"_id_post":idPost}).subscribe(data => {
        let dataPost = data;
        ////console.log(dataLike)
        resolve(dataPost);
      });
  });
}
report_post(idPost)
{
  return new Promise((resolve, reject) => {
    this.httpClient.post<any>(`${this.endpoint}/newReport`,{"_id_post":idPost}).subscribe(data => {
        let dataReport = data;
        ////console.log(dataLike)
        resolve(dataReport);
      });
  });
}
delete_comment(idComment){
  return new Promise((resolve, reject) => {
    this.httpClient.post<any>(`${this.endpoint}/deleteComment`,{"_id_comment":idComment}).subscribe(data => {
        let dataComment = data;
        ////console.log(dataLike)
        resolve(dataComment);
      });
  });
}

delete_reply(idReply){
  return new Promise((resolve, reject) => {
    this.httpClient.post<any>(`${this.endpoint}/deleteReply`,{"_id_reply":idReply}).subscribe(data => {
        let datareply = data;
        ////console.log(dataLike)
        resolve(datareply);
      });
  });
}

post_like(idPost) {
   return new Promise((resolve, reject) => {
    this.httpClient.post<any>(`${this.endpoint}/newLike`,{"_id_post":idPost}).subscribe(data => {
        let dataLike = data;
        ////console.log(dataLike)
        resolve(dataLike);
      });
  });
}
post_unlike(idPost) {
   return new Promise((resolve, reject) => {
    this.httpClient.post<any>(`${this.endpoint}/deleteLike`,{"_id_post":idPost}).subscribe(data => {
        let dataLike = data;
        ////console.log(dataLike)
        resolve(dataLike);
      });
  });
}
get_comments(idPost,skip) {
  return new Promise((resolve, reject) => {
   this.httpClient.post<any>(`${this.endpoint}/comments`,{"_id_post":idPost,"skip":skip}).subscribe(data => {
       let dataComment = data;
       ////console.log(dataLike)
       resolve(dataComment); 
     });
 });
}
get_replies(idComment,skip){ 
  return new Promise((resolve, reject) => {
    this.httpClient.post<any>(`${this.endpoint}/replies`,{"_id_comment":idComment,"skip":skip}).subscribe(data => {
        let dataReplies = data;
        ////console.log(dataLike)
        resolve(dataReplies);
      });
  });
}
get_comments_count(idPost) {
  return new Promise((resolve, reject) => {
   this.httpClient.post<any>(`${this.endpoint}/countComments`,{"_id_post":idPost}).subscribe(data => {
       let dataComment = data;
       ////console.log(dataLike)
       resolve(dataComment);
     });
 });
}

get_replies_count(idComment) {
  return new Promise((resolve, reject) => {
   this.httpClient.post<any>(`${this.endpoint}/countReplies`,{"_id_comment":idComment}).subscribe(data => {
       let dataComment = data;
       ////console.log(dataLike)
       resolve(dataComment);
     });
 });
}

get_likes_count(idPost) {
  return new Promise((resolve, reject) => {
   this.httpClient.post<any>(`${this.endpoint}/countLikes`,{"_id_post":idPost}).subscribe(data => {
       let dataLike = data;
       ////console.log(dataLike)
       resolve(dataLike);
     });
 });
}
get_is_liked(idPost) {
  return new Promise((resolve, reject) => {
   this.httpClient.post<any>(`${this.endpoint}/isLiked`,{"_id_post":idPost}).subscribe(data => {
       let dataLiked = data;
       ////console.log(dataLike)
       resolve(dataLiked);
     });
 });
}


group_members(idGroup){
  return new Promise((resolve, reject) => {
    this.httpClient.post<any>(`${this.endpoint}/groupMembers`,{"_id_group":idGroup}).subscribe(data => {
        let datagroup = data;
        ////console.log(dataLike)
        resolve(datagroup);
      });
  });

}



}
