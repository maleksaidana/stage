import { Injectable } from '@angular/core';
import { CanActivate,Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './../shared/auth.service';

@Injectable({
  providedIn: 'root'
})
export class RoleauthGuard implements CanActivate {
  
  constructor(
    public authService: AuthService,
    public router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      
      const expectedRole = route.data.expectedRole;
      var splitted = expectedRole.split(" ", 2); 

    if (this.authService.isLoggedIn !== true ||   this.authService.getRole()!==splitted[0] && this.authService.getRole()!==splitted[1]) {
      //window.alert("Access not allowed!");
      this.router.navigate(['']);   
    } 

    return true; 
  }
  
}
