import { Component, EventEmitter, Input, Output } from "@angular/core";

import { Option } from "./../../models/option.model";
import { ThemeServiceService} from "../theme-service.service";

@Component({
  selector: 'app-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss']
})
export class ToggleComponent {
  @Input() options: Array<Option>;
  @Output() themeChange: EventEmitter<string> = new EventEmitter<string>();

  constructor(private themeService: ThemeServiceService) {}

  changeTheme(themeToSet) {
    this.themeChange.emit(themeToSet);
  }

}
