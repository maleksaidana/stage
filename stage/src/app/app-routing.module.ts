import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from './shared/auth.service';

import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import { AuthGuard } from "./shared/auth.guard";
import { RoleauthGuard } from "./shared/roleauth.guard";
import { InboxComponent } from './inbox/inbox.component';
import { ProfileComponent } from './profile/profile.component';
import { ClassroomComponent } from './classroom/classroom.component';
import { ClassesComponent } from './classes/classes.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { GroupsComponent } from './groups/groups.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  {path: 'home', component:HomeComponent,canActivate: [AuthGuard]},
  { path: '',   redirectTo: '/login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  {path: 'inbox', component:InboxComponent,canActivate: [AuthGuard]},
  {path: 'profile', component:ProfileComponent,canActivate: [AuthGuard]}, 
  {path: 'profile/:_id', component:ProfileComponent,canActivate: [AuthGuard]}, 
  {path: 'classroom/:id_group', component:ClassroomComponent,canActivate: [AuthGuard]},
  {path: 'classes/:id_semester', component:ClassesComponent,canActivate: [RoleauthGuard],  data: {  expectedRole: 'admin'} },
  {path: 'groups/:id_class', component:GroupsComponent,canActivate: [RoleauthGuard],  data: {  expectedRole: 'admin'} },
  {path: 'dashboard', component:DashboardComponent,canActivate: [RoleauthGuard],  data: {  expectedRole: 'admin'} },
  { path: '**', component: PagenotfoundComponent },  

];
 

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
