import { Component } from '@angular/core';
//import * as jwt_decode from 'jwt-decode';
import { AuthService } from './shared/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent{

  auth:AuthService;
  //logged_in:boolean;
  constructor(auth:AuthService) {   this.auth = auth;}
  
   /*get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('access_token');
    if(authToken!=null && !this.isTokenExpired(authToken))
    return true;
    else
    return false;
  }
  getToken() {
    return localStorage.getItem('access_token');
  }  
  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);

    if (decoded.exp === undefined) return null;

    const date = new Date(0); 
    date.setUTCSeconds(decoded.exp);
    return date;
  }
  isTokenExpired(token?: string): boolean {
    if(!token) token = this.getToken();
    if(!token) return true;

    const date = this.getTokenExpirationDate(token);
    if(date === undefined) return false;
    return !(date.valueOf() > new Date().valueOf());
  }*/
  title = 'stage';
}
