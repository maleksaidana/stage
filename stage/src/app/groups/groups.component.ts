import { Component, OnInit } from '@angular/core';
import { faUsers,faExternalLinkAlt, faLayerGroup} from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute } from '@angular/router';
import {AuthService } from './../shared/auth.service';
import {AdminService } from './../shared/admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})


export class GroupsComponent implements OnInit {
  groupListIcon=faLayerGroup;
  openlinkIcon=faExternalLinkAlt;

  private sub: any;
  auth:AuthService

Classe:any;


  constructor(public rout: Router,auth:AuthService,private route: ActivatedRoute, public adminservice: AdminService) { this.auth=auth}

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
    //  this.id_class = params['id_class']; 
      this.Classe=this.classe(params['id_class']);

    
   });
   
  }


  classe(_id){
    var classe:any;
    this.adminservice.get_classe({"_id":_id}).then(data =>{
      classe=data;
       // //console.log('postcomment =', comm_res);
        if(classe)
        {
          
       
          this.Classe=classe;
          this.Classe=this.Classe.u[0];
          
       //  //console.log("classseeeeee",this.Classe.groups);

       var group_id="5f4bfe1239f94a2270fa6587";

      // var group:any=this.Classe.groups.find(x => x._id === group_id);
      for(let group of this.Classe.groups){
        var teacher:any=group.group.student.find(x => x.role === "teacher");
        teacher.profile_pic=this.auth.endpoint+"/uploads/profile_images/"+teacher._id+".jpg";
        group.teacher=teacher;
       
      }

     // //console.log("teachehher",this.Classe); 
        }
    });



  }



  classroom(id)
  {
    this.rout.navigate(['classroom/'+id]);
  }

}
