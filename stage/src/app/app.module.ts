import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TextareaAutosizeModule } from 'ngx-textarea-autosize';
import { ToggleComponent } from './toggle/toggle.component';
import { AppMaterialModule } from "./app-material/app-material.module";

import { HttpModule } from '@angular/http';
import { MenuLoginComponent } from './menu-login/menu-login.component';
import { BgLoginComponent } from './bg-login/bg-login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AuthInterceptor } from './shared/authconfig.interceptor';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InboxComponent } from './inbox/inbox.component';
import { ProfileComponent } from './profile/profile.component';
import { ClassroomComponent } from './classroom/classroom.component';
import { ClassesComponent } from './classes/classes.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { TooltipModule } from 'ng2-tooltip-directive';
import { QuickaccessComponent } from './quickaccess/quickaccess.component';
import { GroupsComponent } from './groups/groups.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SearchComponent } from './search/search.component';

import { SearchService } from './shared/search.service';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    FooterComponent,
    LoginComponent,
    HomeComponent,
    SidebarComponent,
    ToggleComponent,
    MenuLoginComponent,
    BgLoginComponent,
    InboxComponent,
    ProfileComponent,
    ClassroomComponent,
    ClassesComponent,
    PagenotfoundComponent,
    QuickaccessComponent,
    GroupsComponent,
    DashboardComponent,
    SearchComponent    
    ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    TextareaAutosizeModule,
    HttpClientModule,
    HttpModule,
    HttpClientModule,
    AppMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    TooltipModule,
    FontAwesomeModule
  ],

  providers: [{
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true},SearchService
      //{provide: LocationStrategy, useClass: HashLocationStrategy}
    ],
    exports:[
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
