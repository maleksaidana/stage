import { Component, OnInit ,AfterViewInit  } from '@angular/core';
import { faTrash,faFile,faNewspaper,faUser,faUserEdit, faPaperPlane, faPlus, faReply,faThumbsUp,faComments,faEllipsisH,faEyeSlash,faMinusCircle,faClock, faPhone, faEnvelope, faBirthdayCake, faHashtag, faGraduationCap, faStream, faCheck} from '@fortawesome/free-solid-svg-icons';
import {InfService } from './../shared/inf.service';
import { ActivatedRoute,Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import * as $ from 'jquery';
import {HostListener }  from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit  {
  deleteIcon=faTrash;
  profileIcon= faUser;
  editIcon= faUserEdit;
  sendIcon= faPaperPlane;
  moreIcon= faPlus;
  replyIcon= faReply;
  likeIcon= faThumbsUp;
  commentIcon= faComments;
  optionsIcon= faEllipsisH;
  hideIcon= faEyeSlash;
  reportIcon= faMinusCircle;
  timeIcon= faClock;
  newsfeedIcon= faNewspaper;
  phoneIcon=faPhone;
  emailIcon=faEnvelope;
  birthdateIcon=faBirthdayCake;
  daIcon=faHashtag;
  programIcon=faGraduationCap;
  timelineIcon=faStream;
  fileIcon=faFile;
  confirmIcon=faCheck;
  
  user:any;
  posts:any;
  countcomm=0;
  url;
  msg = "";
  event;
  email:any;
  phone:any;
  password:any;
  err_password:any;
_id_user;
post_type="3";

 get Email(){
    return this.userEdit.get('Email')
    }

  get Phone(){
  return this.userEdit.get('Phone')
  }
  get Password(){
  return this.userEdit.get('Password')
  }
  get NewPassword(){
    return this.userEdit.get('NewPassword')
    }
  get ConfirmNewPassword(){
    return this.userEdit.get('ConfirmNewPassword')
    }
  userEdit = new FormGroup({
    Email: new FormControl('',[	
      Validators.required,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
      Password: new FormControl('',[	
      Validators.required,
      Validators.minLength(6)]),
      Phone: new FormControl('',[
      Validators.required,
      Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")
      ]),
      NewPassword: new FormControl('',[	
        Validators.required,
        Validators.minLength(6)]),
      ConfirmNewPassword: new FormControl('',[	
        Validators.required,
        Validators.minLength(6),this.passwordsMatch()])
    }); 
    
    
    passwordsMatch(){     
      return (control: any):{[key: string]: boolean} | null => {
        var newpass=$("#newpassword").val();
      //console.log("match",newpass)
      if( control.value !==null && (isNaN(control.value) || control.value !=newpass)){
        return {'passwordsMatch': true}
      }
      return null;
    };
    }
   

  private canRender = false;

  constructor(private router: ActivatedRoute,public infoservice: InfService,public route: Router) {   }
  
  myprofile:boolean=false;
  profile_pic=this.infoservice.authService.endpoint+"/uploads/profile_images/";
  profile_pic_target=this.infoservice.authService.endpoint+"/uploads/profile_images/";
  post_file=this.infoservice.authService.endpoint+"/uploads/file_posts/";
  post_images=this.infoservice.authService.endpoint+"/uploads/images_posts/";
  ngOnInit(): void { 

    // //console.log("counttt",this.countcomm)

    this.router.paramMap.subscribe( paramMap => {
      this.profile_pic=this.infoservice.authService.endpoint+"/uploads/profile_images/"+paramMap.get('_id')+".jpg";
     if(paramMap.get('_id')==null|| paramMap.get('_id')===this.infoservice.authService.get_id() )
     {
      
       this._id_user=this.infoservice.authService.get_id();
       this.profile_pic=this.infoservice.authService.endpoint+"/uploads/profile_images/"+this.infoservice.authService.get_id()+".jpg";
      this.myprofile=true;
      this.infoservice.getUserInfo(this.infoservice.authService.get_id()).then(dataR => {
        
        this.user=dataR.user[0];
        this.email=this.user.email;
        this.phone=this.user.phone;
        //console.log('data =', this.user);
      

      });
      

      this.infoservice.getPosts(this.infoservice.authService.get_id(),0,"0",this.post_type).then(dataPosts =>{
          this.posts=dataPosts;
          this.posts=this.posts.post;
          this.getPostsCount();
          this.getpostinfos();
          this.posts.skip=0;
          //console.log('asba =', this.posts);
      });
     }
     else{
      this._id_user=paramMap.get('_id');
      this.infoservice.getPosts(paramMap.get('_id'),0,"0",this.post_type).then(dataPosts =>{
        this.posts=dataPosts;
        this.posts=this.posts.post;
        this.getPostsCount();
        this.getpostinfos();
        this.posts.skip=0;
        //console.log('asba =', this.posts);
    });
      this.infoservice.getUserInfo(paramMap.get('_id')).then(dataR => {
        
        $('.edit-btn').remove();
      $('.file').remove();
        this.user=dataR.user[0];
        //console.log('data =', this.user);
      })
      .catch((err) => { 
        this.infoservice.authService.router.navigate(['pagenotfound']); 
    });
     }
     
   
     ////console.log(this.user);
  })
    //this.infoservice.infos("5f401375afc2f103a045e0da");
   
  }
  @HostListener("window:scroll", ["$event"])
  onWindowScroll() {
  let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
  let max = document.documentElement.scrollHeight;  
  
   if(pos >= max-200)   {
    
    $('#pageLoad').removeClass('d-none');
    if(this.posts.skip>this.posts.totalPosts){
       
       $('#pageLoad').addClass('d-none');
    }
    this.posts.skip= this.posts.skip+3; 
    this.appendPosts(this.posts.skip);

   }
  }
  upload(event) {
    this.event=event;
    if(!event.target.files[0] || event.target.files[0].length == 0) {
      this.msg = 'You must select an image';
      window.alert(this.msg);
			return;
		}
		
		var mimeType = event.target.files[0].type;
		
		if (mimeType.match(/image\/*/) == null) {
      this.msg = "Only images are supported";
      window.alert(this.msg);
			return;
		}
		
		var reader = new FileReader();
		reader.readAsDataURL(event.target.files[0]);
		
		reader.onload = (_event) => {
			this.msg = "";
			this.url = reader.result; 
		}
    this.showModal()
  }
  uploadClick(){
    document.getElementById("uploadbtn").click();
  }
  
  showModal()
  {
    document.getElementById("openModalButton").click();
  }
  showConfirmModal()
  {
    document.getElementById("openConfirmModalButton").click();
    setTimeout(function() {$('#confirmmodalbtn').click();}, 1000);

  }
  onSubmitImageForm(){
    this.infoservice.upload(this.event);
    
    this.profile_pic=this.url;
  }
  onSubmitEditForm(){
    var password = this.Password.value;
    var email = this.Email.value;
    var phone = this.Phone.value;
    var newpassword = this.NewPassword.value;
    var confirmnewpassword = this.ConfirmNewPassword.value;
    if(this.ConfirmNewPassword.errors?.minlength||this.ConfirmNewPassword.errors?.passwordsMatch||this.NewPassword.errors?.minlength||this.Password.errors?.minlength||this.Password.errors?.required||this.Phone.errors?.pattern||this.Phone.errors?.required||this.Email.errors?.pattern||this.Email.errors?.required)
    {
      //alert("fuckyou")
      return;
    }
    var verify_res;
    this.infoservice.verify_password(password).then(dataVerify =>{
      verify_res=dataVerify;
     // //console.log('postcomment =', comm_res);
      if(verify_res.verify==1)
      {
        var edit_res;
        this.infoservice.edit_profile(email,phone,password,newpassword,confirmnewpassword).then(dataEdit =>{
            edit_res=dataEdit;
            if(edit_res)
            {
              $("#edit_cancel_btn").click();
              this.user.email=email;
              this.user.phone=phone;
              if(newpassword.toString().trim()!=""){
              this.user.password=newpassword;
              }
            }
        });
      }
      else{
        this.err_password="The password you've entered is incorrect";
        return;
      }
    });
  
  }
  getDate(d)
  {
    let newDate = new Date(d);
    return this.timeSince(newDate);

  }

  timeSince(date) {
    var seconds = Math.floor((Date.now() - date) / 1000);
    
    var interval = seconds / 31536000;
  
    if (interval > 1) {
      return Math.floor(interval) + " years ago";
    }
    interval = seconds / 2592000;
    if (interval > 1) {
      return Math.floor(interval) + " months ago";
    }
    interval = seconds / 86400;
    if (interval > 1) {
      return Math.floor(interval) + " days ago";
    }
    interval = seconds / 3600;
    if (interval > 1) {
      return Math.floor(interval) + " hours ago";
    }
    interval = seconds / 60;
    if (interval > 1) {
      return Math.floor(interval) + " minutes ago";
    }
    return Math.floor(seconds) + " seconds ago";
  }





 upperCase(s)
 {
  var s_string:String=String(s);
  return s_string.charAt(0).toUpperCase()+s_string.slice(1)
 }
 
  getImagesFromString(i,idPost)
 {
   
  var files_string=""+i;
  if(files_string=="")
 {return;}

  var splitted=files_string.split(";");

 var images:Array<{iterator: String, url: string}> = [];
  for(var j=0;j<splitted.length-1;j++)
  {
    images.push({url:this.post_images+splitted[j].toString(),iterator:j.toString()});
  }
  var post:any=this.posts.find(x => x._id === idPost);

   post.images=images;
 }

 getFilesFromString(f,idPost)
 {
  var files_string=""+f;
  if(files_string=="")
  {return;}
  var splitted=files_string.split(";");
  
  var files:Array<{url: String, name: string}> = [];
  for(var i=0;i<splitted.length-1;i++)
  {
   files.push({name: splitted[i].toString(),url:this.post_file+splitted[i].toString()});
  }
  var post:any=this.posts.find(x => x._id === idPost);

  post.files=files;
 }

 countComments(_id){
  var comm:any;
  this.infoservice.http.post<any>(`${this.infoservice.endpoint}/countComments`,{"_id_post":_id}).toPromise().then(data =>{
    comm=data.comm;
    //console.log(comm)
  });;
 }




 checkIdUser(idUser):boolean
{
  
  return (idUser==this.infoservice.authService.get_id())?true:false;
}
deletePost(idPost){
  var result = confirm("Are you sure?");
  if (!result) {
      return;
  }
  var comment_res:any;
  this.infoservice.delete_post(idPost).then(dataPost =>{
    comment_res=dataPost;
      ////console.log('postcomment =', like_res);
      if(comment_res)
      {
          var post:any=this.posts.find(x => x._id === idPost);
          var posts:any=this.posts;
          ////console.log("finall1",this.posts)
          var p = posts.indexOf(post);
          posts.splice(p,1);
          this.showConfirmModal()
          ////console.log("finall2",this.posts);
  
      }
  });
}
reportPost(idPost)
{
  var result = confirm("Are you sure?");
  if (!result) {
      return;
  }
  var report_res:any;
  this.infoservice.report_post(idPost).then(dataPost =>{
    report_res=dataPost;
      ////console.log('postcomment =', like_res);
      if(report_res)
      {
        window.alert("Your report has been succesfully submitted");
      }
  });
}
deleteComment(idComment,idPost){
  var result = confirm("Are you sure?");
  if (!result) {
      return;
  }
  var comment_res:any;
  this.infoservice.delete_comment(idComment).then(dataComment =>{
    comment_res=dataComment;
      ////console.log('postcomment =', like_res);
      if(comment_res)
      {
        var post:any=this.posts.find(x => x._id === idPost);
        var comments:any=post.comment
        var comment:any=comments.find(x=>x._id ===idComment)
        //console.log("finall",comment)
        var c = comments.indexOf(comment);
        comments.splice(c,1);
        var tot=parseInt(($('#tot'+idPost).html()).toString()) -1;
        $('#tot'+idPost).html(""+tot);
        this.showConfirmModal()
        //post.totalComments=post.totalComments-1;
      }
  });
}

deleteReply(idReply,idComment,idPost){
  var result = confirm("Are you sure?");
  if (!result) {
      return;
  }
  
  var reply_res:any;
  this.infoservice.delete_reply(idReply).then(dataReply =>{
    reply_res=dataReply;
      ////console.log('postcomment =', like_res);
      if(reply_res)
      {
        var post:any=this.posts.find(x => x._id === idPost);
        var comments:any=post.comment
        var comment:any=comments.find(x=>x._id ===idComment)
        var replies:any=comment.reply
        var reply:any=replies.find(x=>x._id ===idReply)
        //console.log("finall",comment)
        var r = replies.indexOf(reply);
        replies.splice(r,1);
        this.showConfirmModal()
      // comment.totalReplies=comment.totalReplies-1;
      }
  });
}



 postComment(idPost)
 {
   var content = $('#textarea_'+idPost).val();
    if(content=="")
    {
      window.alert("you must enter something");
      return;
    }
    var comm_res:any;
  this.infoservice.post_comment(idPost,content).then(dataComment =>{
      comm_res=dataComment;
     // //console.log('postcomment =', comm_res);
      if(comm_res)
      {
        var post:any=this.posts.find(x => x._id === idPost);
        ////console.log("finall",post)
        comm_res.firstname=this.infoservice.authService.get_firstname();
        comm_res.lastname=this.upperCase(this.infoservice.authService.get_lastname());
        post.comment.push(comm_res);
        ////console.log(this.posts)
        //post.totalComments=post.totalComments+1;
        var tot=parseInt(($('#tot'+idPost).html()).toString()) +1;
        $('#tot'+idPost).html(tot+"");
        $('#collapseCommentsToggle_'+idPost).click();
        $('#textarea_'+idPost).val("");
      }
  });
 }


 postReply(idComment,idPost)
 {
   var content = $('#textarea_'+idComment).val();
    if(content=="")
    {
      window.alert("you must enter something");
      return;
    }
    var reply_res:any;
  this.infoservice.post_reply(idComment,idPost,content).then(dataReply =>{
      reply_res=dataReply;
     // //console.log('postcomment =', comm_res);
      if(reply_res)
      {
        var post:any=this.posts.find(x => x._id === idPost);
        var comments:any=post.comment;
        
        var comment:any=comments.find(x => x._id === idComment);

        reply_res.firstname=this.infoservice.authService.get_firstname();
        reply_res.lastname=this.upperCase(this.infoservice.authService.get_lastname());
        comment.reply.push(reply_res);
        $('#textarea_'+idComment).val("");
      }
  });
 }


 like(idPost)
 {

    var like_res:any;
  this.infoservice.post_like(idPost).then(dataLike =>{
    like_res=dataLike;
      ////console.log('postcomment =', like_res);
      if(like_res)
      {
        var post:any=this.posts.find(x => x._id === idPost);
        ////console.log("finall",post)
        post.is_liked="1";
        post.totalLikes=post.totalLikes+1;
      }
  });
 }
 unlike(idPost)
 {
    var like_res:any;
  this.infoservice.post_unlike(idPost).then(dataLike =>{
    like_res=dataLike;
      ////console.log('postcomment =', like_res);
      if(like_res)
      {
        var post:any=this.posts.find(x => x._id === idPost);
        ////console.log("finall",post)
        post.is_liked="0";
        post.totalLikes=post.totalLikes-1;
      }
  });
 }
getpostinfos()
{
  for(let post of this.posts)
  {
    this.getComments(post._id);
    this.getCommentsCount(post._id);
    this.getLikesCount(post._id);
    this.getIsLiked(post._id);
    this.getImagesFromString(post.image,post._id);
    this.getFilesFromString(post.file,post._id);
  }
}



getComments(idPost)
{
   var comm_res:any;
 this.infoservice.get_comments(idPost,0).then(dataComment =>{
     comm_res=dataComment;
    // //console.log('postcomment =', comm_res);
     if(comm_res)
     {
       var post:any=this.posts.find(x => x._id === idPost);

       post.comment=comm_res.comment;
       post.comment.skip=5;
       ////console.log(this.posts)
      /* for(let c of comm_res.comment){
        post.comment.push(c);
      }*/
     }
 });
}



appendComments(idPost,skip)
{
   var comm_res:any;
 this.infoservice.get_comments(idPost,skip).then(dataComment =>{
     comm_res=dataComment;
    // //console.log('postcomment =', comm_res);
     if(comm_res)
     {
       var post:any=this.posts.find(x => x._id === idPost);
     for(let c of comm_res.comment){
        post.comment.push(c);
        
      }
      post.comment.skip= post.comment.skip+5;
     }
 });
}

appendPosts(skip)
{
 
  var post:any;
  if(this.posts.skip<this.posts.totalPosts){
  


 this.infoservice.getPosts(this._id_user,skip,"0",this.post_type).then(dataPosts =>{

  post=dataPosts;
  for(let p of post.post){
    this.posts.push(p);
  }
  $('#pageLoad').addClass('d-none');
  this.getPostsCount();
  this.getpostinfos();
 // this.posts.skip= this.posts.skip+3;
  //console.log('asba2 =', this.posts);
});

}

}
getReplies(idPost,idComment)
{
  ////console.log('comm',idComment)
   var reply_res:any;
   this.infoservice.get_replies(idComment,0).then(dataReplies =>{
    reply_res=dataReplies;
    // //console.log('postcomment =', comm_res);
     if(reply_res)
     {
      var post:any=this.posts.find(x => x._id === idPost);
       var comments:any=post.comment;
       
       var comment:any=comments.find(x => x._id === idComment);
       ////console.log('rep',reply_res)
       comment.reply=reply_res.reply;
       comment.skip=3;
       this.getRepliesCount(idComment,post._id);
       ////console.log(this.posts)
     }
 });
}


appendReplies(idPost,idComment,skip)
{
  ////console.log('comm',idComment)
   var reply_res:any;
   this.infoservice.get_replies(idComment,skip).then(dataReplies =>{
    reply_res=dataReplies;
    // //console.log('postcomment =', comm_res);
     if(reply_res)
     {
      var post:any=this.posts.find(x => x._id === idPost);
       var comments:any=post.comment;
       
       var comment:any=comments.find(x => x._id === idComment);
       ////console.log('rep',reply_res)

       for(let c of reply_res.reply){
        comment.reply.push(c);
        
      }
      comment.skip= comment.skip+3;
      // comment.reply=reply_res.reply;
     }
 });
}
 


getPostsCount()
{
   var count_res:any;
 this.infoservice.get_posts_count(this._id_user,"0",this.post_type).then(dataCount =>{
   count_res=dataCount;
    // //console.log('postcomment =', comm_res);
     if(count_res)
     {
       
  
       this.posts.totalPosts=count_res.pos;
       ////console.log(this.posts)
     }
 });
}


getCommentsCount(idPost)
{
   var comm_count_res:any;
 this.infoservice.get_comments_count(idPost).then(dataCommentCount =>{
  comm_count_res=dataCommentCount;
    // //console.log('postcomment =', comm_res);
     if(comm_count_res)
     {
       var post:any=this.posts.find(x => x._id === idPost);
    
       post.totalComments=comm_count_res.comm;
       ////console.log(this.posts)
     }
 });
}



getRepliesCount(idComment,idPost)
{
   var comm_count_res:any;
 this.infoservice.get_replies_count(idComment).then(dataCommentCount =>{
  comm_count_res=dataCommentCount;
    // //console.log('postcomment =', comm_res);
     if(comm_count_res)
     {
       var post:any=this.posts.find(x => x._id === idPost);
    

       var post:any=this.posts.find(x => x._id === idPost);
       var comments:any=post.comment;
       
       var comment:any=comments.find(x => x._id === idComment);
       ////console.log('rep',reply_res)

       comment.totalReplies=comm_count_res.rep;
       //console.log("hahaha",this.posts)
     }
 });
}


getLikesCount(idPost)
{
   var like_res:any;
 this.infoservice.get_likes_count(idPost).then(dataLike =>{
  like_res=dataLike;
    // //console.log('postcomment =', comm_res);
     if(like_res)
     {
       var post:any=this.posts.find(x => x._id === idPost);

       post.totalLikes=like_res.like;
       ////console.log(this.posts)
     }
 });
}
getIsLiked(idPost)
{
  var like_res:any;
 this.infoservice.get_is_liked(idPost).then(dataLike =>{
  like_res=dataLike;
    // //console.log('postcomment =', comm_res);
     if(like_res)
     {
       var post:any=this.posts.find(x => x._id === idPost);

       post.is_liked=like_res.liked;
       ////console.log(this.posts)
     }
 });
}

visitProfile(idUser)
{
  this.route.navigate(['profile/'+idUser]);
}

}


