import { Component, OnInit } from '@angular/core';
import { faInfoCircle,faSearch, faBell,faTrash } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from '../shared/auth.service';
import * as $ from 'jquery'

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  infoIcon = faInfoCircle;
  searchIcon = faSearch;
  notificationIcon = faBell;
  notificationDeleteIcon = faTrash;
  
  constructor(    public authService: AuthService) { }

  ngOnInit(): void {
    if (this.authService.isLoggedIn) {
     if(this.authService.isAdmin){
      $('.head').addClass("bg-info");
     }  
     else if(this.authService.isTeacher)
     {
      $('.head').addClass("bg-warning");

     }
   }
  }

}
