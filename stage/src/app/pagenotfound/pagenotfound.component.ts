import { Component, OnInit } from '@angular/core';
import{faHome, faTools} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-pagenotfound',
  templateUrl: './pagenotfound.component.html',
  styleUrls: ['./pagenotfound.component.scss']
})
export class PagenotfoundComponent implements OnInit {
  homeIcon=faHome;
  supportIcon=faTools;
  constructor() { }

  ngOnInit(): void {
  }

}
