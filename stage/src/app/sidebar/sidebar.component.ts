import { Component, OnInit } from '@angular/core';
import{faNewspaper,faUser,faGraduationCap,faInbox,faUsers,faPowerOff, faCogs} from '@fortawesome/free-solid-svg-icons';
import { AuthService } from './../shared/auth.service';
import {InfService } from './../shared/inf.service';
import { ActivatedRoute ,Router} from '@angular/router';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  newsfeedIcon= faNewspaper;
  signoutIcon= faPowerOff;
  profileIcon= faUser;
  inboxIcon= faInbox;
  dashboardIcon=faCogs
  
  user:any;
  public showOverlay = true;
  constructor(public authService: AuthService,private router: ActivatedRoute,public infoservice: InfService,public route: Router) { this.authService =authService;  }
 
profile_pic=this.authService.endpoint+"/uploads/profile_images/"+this.authService.get_id()+".jpg";
  ngOnInit(): void {
      this.infoservice.getUserInfo(this.infoservice.authService.get_id()).then(dataR => {
        
        this.user=dataR.user[0];
        ////console.log('data =', this.user);

      });
  }

  logout() {
    this.authService.doLogout()
  }
  visitProfile(idUser)
{
  this.route.navigate(['profile/'+idUser]);
}
}
