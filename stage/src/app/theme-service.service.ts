import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { StyleManagerService } from "./style-manager.service";

import { Option } from "./../models/option.model";
@Injectable({
  providedIn: 'root'
})
export class ThemeServiceService {
  
  constructor(private styleManager: StyleManagerService) {   }
  getThemeOptions(): Array<Option> {
    let options: Option[] = [
      {
        "backgroundColor": "#fff",
        "buttonColor": "#ffc107",
        "headingColor": "#673ab7",
        "label": "Deep Purple & Amber",
        "value": "deeppurple-amber"
      },
      {
        "backgroundColor": "#fff",
        "buttonColor": "#ff4081",
        "headingColor": "#3f51b5",
        "label": "Indigo & Pink",
        "value": "indigo-pink"
      },
      {
        "backgroundColor": "#303030",
        "buttonColor": "#607d8b",
        "headingColor": "#e91e63",
        "label": "Pink & Blue Grey",
        "value": "pink-bluegrey"
      },
      {
        "backgroundColor": "#303030",
        "buttonColor": "#4caf50",
        "headingColor": "#9c27b0",
        "label": "Purple & Green",
        "value": "purple-green"
      }
  ];
    return options;
  }

  setTheme(themeToSet) {
    this.styleManager.setStyle(
      "theme",
      `C:/Users/rayow/Downloads/BattleShip/stage/stage/node_modules/@angular/material/prebuilt-themes/${themeToSet}.css`
    );
  }
}
