import { Component, OnInit } from "@angular/core"
import { SearchService } from "../shared/search.service"
import { FormControl } from "@angular/forms"
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute,Router } from '@angular/router';

import "rxjs/add/operator/debounceTime"
import "rxjs/add/operator/distinctUntilChanged"
import "rxjs/add/operator/switchMap"
@Component({
 selector: "app-search",
 templateUrl: "./search.component.html",
 styleUrls: ["./search.component.scss"]
})
export class SearchComponent implements OnInit {
results: any[] = [];
searchIcon = faSearch;
linkimage=this._searchService.authService.endpoint+"/uploads/profile_images/";

 queryField: FormControl = new FormControl();
 constructor(private _searchService: SearchService,public route: Router) { }

 ngOnInit() {
    this.queryField.valueChanges
  .debounceTime(350)
  .distinctUntilChanged()
  .switchMap((query) =>  this._searchService.search(query))
    .subscribe( result => { if (result.json().search == "") {this.results=[]; return; } else {  this.results = result.json().search;}
  });
  }
  
  visit_profile(_id)
  {
    this.results=[]
    this.queryField.setValue("")
    this.route.navigate(['profile/'+_id]);
  }
}
