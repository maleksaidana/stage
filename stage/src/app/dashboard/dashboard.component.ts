import { Component, OnInit } from '@angular/core';

import { faUsers, faClipboardList, faPlus, faPencilAlt, faTrash, faMinusCircle, faUser, faBirthdayCake, faGraduationCap, faLock, faKey, faCogs, faCheck, faEdit, faChartLine, faNewspaper} from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute } from '@angular/router';
import {AuthService } from './../shared/auth.service';
import {AdminService } from './../shared/admin.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  auth:AuthService
  addIcon=faPlus
  editIcon=faPencilAlt
  removeIcon=faTrash
  reportsIcon=faMinusCircle
  userIcon=faUser
  birthdateIcon=faBirthdayCake
  programIcon=faGraduationCap
  idIcon=faLock
  passwordIcon=faKey
  classIcon=faUsers
  dashboardIcon=faCogs
  checkIcon=faCheck
  descriptionIcon=faEdit
  sessionsIcon =faClipboardList;
  statisticsIcon=faChartLine
  postsIcon=faNewspaper

  url;
  msg = "";
  event;
  idClass:any;

  users:Array<{"_id":String,"da":String,"name":String,"role":String}>=[]
  usersEdit:Array<{"_id":String,"da":String,"name":String,"role":String}>=[]
  EditedClass:Array<any>=[]
adminservice:AdminService
  semesters:any
  class_pic:any
  post_count:any;
  user_count:any;

  constructor(auth:AuthService,private route: ActivatedRoute,adminservice:AdminService) {this.adminservice=adminservice; this.auth=auth}

  ngOnInit(): void {
    this.adminservice.get_sessions().then(sessions => {
      if(sessions)
      {
        this.semesters=sessions;
        this.semesters=this.semesters.u;
        }
    });
    this.adminservice.get_posts_count().then(dataPosts => {
      if(dataPosts)
      {
        this.post_count=dataPosts;
        //console.log(this.post_count)
        }
    });
    this.adminservice.get_users_count().then(dataUsers => {
      if(dataUsers)
      {
        this.user_count=dataUsers;
        }
    });

  }
  remove(_id)
  {
    var user:any=this.users.find(x => x._id === _id);
    var users:any=this.users;
    var u = users.indexOf(user);
    users.splice(u,1);
  }
  removeEdit(_id)
  {
    var user:any=this.usersEdit.find(x => x._id === _id);
    var users:any=this.usersEdit;
    var u = users.indexOf(user);
    users.splice(u,1);
  }
  removeGroup(_id)
  {
    var groups:any=this.EditedClass;
    groups=groups.groups
    var group:any=groups.find(x => x._id === _id);
    var g= groups.indexOf(group);
    groups.splice(g,1);
  }
  
  add()
  {
    var da=$('#IdentificatorAdd').val();
    if(this.users.find(x => x.da === da))
    {
      alert("user exists!")
      return;
    }
    this.adminservice.getUserInfo(da).then(dataR => {
        
      var user=dataR.user[0];
      if(user?._id)
      {    
        if(this.users.find(x => x.role === "teacher") && user.role=='teacher')
        {
          alert("you can't add two teachers")
          return;
        }
        if(user.role=='admin')
        {
          window.alert("you can't add an administrator")
          return;
        }
        var u={"_id":user._id,"da":user.da,"name":user.lastname+' '+user.firstname,"role":user.role}
        this.users.push(u);
        $('#IdentificatorAdd').val("")
      }
      else{
        window.alert('user not found')
        return
      }
    })
   
  }
  addEdit()
  {
    var da=$('#IdentificatorEditClass').val();
    var groups:any=this.EditedClass;
    groups=groups.groups
    if(this.usersEdit.find(x => x.da === da))
    {
      alert("user exists!")
      return;
    }
    this.adminservice.getUserInfo(da).then(dataR => {
        
      var user=dataR.user[0];
      if(user?._id)
      {    
        if(this.usersEdit.find(x => x.role === "teacher") && user.role=='teacher')
        {
          alert("you can't add two teachers")
          return;
        }
        for(let group of groups)
        {
          var g:any=group.group
          ////console.log(g)
          if(g.student.find(x => x._id === user._id))
          {
            alert("user exists in other groups!")
            return;
          }
        }
        if(user.role=='admin')
        {
          window.alert("you can't add an administrator")
          return;
        }
        var u={"_id":user._id,"da":user.da,"name":user.lastname+' '+user.firstname,"role":user.role}
        this.usersEdit.push(u);
        $('#IdentificatorEditClass').val("")
      }
      else{
        window.alert('user not found')
        return
      }
    })
  }
  appendGroup(idClass)
  {
    var idclass=idClass
   if(this.usersEdit.length==0)
    {
      alert('you forgot something')
    }
   
    this.adminservice.add_group(this.usersEdit,idclass).then(dataR => {
      this.showConfirmModal()
     var classe:any=dataR;
     classe=classe.u[0];
     var groups:Array<any>=classe.groups
     var groups2:any=this.EditedClass
     groups2=groups2.groups
     for(let group of groups)
     {
       if(!(groups2.find(x => x._id === group._id)))
       {
        groups2.push(group)
       }
     }
     this.usersEdit=[]
     //this.EditedClass=classe
    });
    
  }
  removeEditClass(idGroup,iduser)
  {
    
    var groups:any=this.EditedClass;
        groups=groups.groups
        var group:any=groups.find(x => x._id === idGroup);
        var g:any=group.group
    var users:any=g.student;
    var user:any=users.find(x => x._id === iduser);
    var u = users.indexOf(user);
    users.splice(u,1);
  }
  addEditClass(idGroup)
  {
    
    var da=$('#IdentificatorEditClassAdd_'+idGroup).val();
    var groups:any=this.EditedClass;
        groups=groups.groups
        var group:any=groups.find(x => x._id === idGroup);
        var g:any=group.group
        if(g.student.find(x => x.da === da))
        {
          alert("user exists!")
          return;
        }
    this.adminservice.getUserInfo(da).then(dataR => {
        
      var user=dataR.user[0];
      if(user?._id)
      {    
        var group:any=groups.find(x => x._id === idGroup);
        var g:any=group.group
        //console.log(g)
        if(g.student.find(x => x.role === "teacher") && user.role=='teacher')
        {
          alert("you can't add two teachers")
          return;
        }
        for(let group of groups)
        {
          var g:any=group.group
          ////console.log(g)
          if(g.student.find(x => x._id === user._id))
          {
            alert("user exists in other groups!")
            return;
          }
        }
        if(user.role=='admin')
        {
          window.alert("you can't add an administrator")
          return;
        }
        var u={"_id":user._id,"da":user.da,"lastname":user.lastname,"firstname":user.firstname,"role":user.role}
        
       
        g.student.push(u)
        //this.users.push(u);
        $('#IdentificatorEditClassAdd_'+idGroup).val("")
      }
      else{
        window.alert('user not found')
        return
      }
    })
   
  }
  addClass()
  {
    var name=$('#Classname').val();
    var description=$('#Classdescription').val();
    var semester=$('#addsemestertoclass').val()
    if(name==""||this.users.length==0||description=="")
    {
      alert('you forgot something')
      return;
    }
    this.adminservice.add_class(name,description,this.users,semester).then(dataR => {
      this.showConfirmModal()
      $('#Classname').val("");
      $('#Classdescription').val("");
      this.users=[]
     ////console.log(dataR);
    })
   
  }
  addSemester()
  {
    var name=$('#Semestername').val();
    var description=$('#Semesterdescription').val();
    if(name==""||description=="")
    {
      alert('you forgot something')
      return;

    }
    this.adminservice.add_semester(name,description).then(dataR => {
      this.showConfirmModal()
      $('#Semestername').val("");
      $('#Semesterdescription').val("");
      ////console.log(dataR);
    })
   
  }

  addUser()
  {
    var firstname=$('#Firstname').val();
    var lastname=$('#Lastname').val();
    var birthdate=$('#Birthdate').val();
    var identificator=$('#Identificator').val();
    var program=$('#Program').val();
    var password=$('#Password').val();
    var role=$("#myform input[type='radio']:checked").val();
    if(!role || firstname=="" || lastname=="" || birthdate=="" || identificator=="" || program==""|| password=="")
    {
      alert("you forgot something")
      return
    }
    this.adminservice.add_user(firstname,lastname,birthdate,identificator,program,password,role).then(dataR => {
        
      $('#Firstname').val("");
      $('#Lastname').val("");
      $('#Birthdate').val("");
      $('#Identificator').val("");
      $('#Program').val("");
      $('#Password').val("");
      
      this.showConfirmModal()
    });

  }
  editUser()
  {
    var firstname=$('#EditFirstname').val();
    var lastname=$('#EditLastname').val();
    var birthdate=$('#EditBirthdate').val();
    var identificator=$('#IdentificatorEdit').val();
    var program=$('#EditProgram').val();
    var id=$('#EditIdUser').val();
    var role=$('#EditRoleUser').val();
    if(firstname=="" || lastname=="" || birthdate=="" || identificator=="" || program=="")
    {
      alert("you forgot something")
      return
    }
    this.adminservice.edit_user(id,role,firstname,lastname,birthdate,identificator,program).then(dataR => {
    $('#EditFirstname').val("");
    $('#EditLastname').val("");
    $('#EditBirthdate').val("");
    $('#IdentificatorEdit').val("");
    $('#EditProgram').val("");
    $('#EditRoleUser').val("");
    $('#EditIdUser').val("");
    this.showConfirmModal()
  });

  }

  removeUser()
  {
    var id=$('#EditIdUser').val();
    if(id=="")
    {
      alert("you forgot something")
      return
    }
    this.adminservice.remove_user(id).then(dataR => {
      $('#EditFirstname').val("");
    $('#EditLastname').val("");
    $('#EditBirthdate').val("");
    $('#IdentificatorEdit').val("");
    $('#EditProgram').val("");
    $('#EditRoleUser').val("");
    $('#EditIdUser').val("");
      this.showConfirmModal()
    });
  }
  editClass()
  {
    var classdescription=$('#ClassdescriptionEdit').val();
    var semester=$('#editsemesterofclass').val();
    if(classdescription=="" || semester=="" || this.EditedClass.length==0)
    {
      alert("you forgot something")
      return;
    }
    this.adminservice.edit_class(classdescription,semester,this.EditedClass).then(dataR => {
      $('#ClassdescriptionEdit').val("");
      this.EditedClass=[]
      this.showConfirmModal()
    });

    //to do $('#ClassdescriptionEdit').val("");
  }
  checkUser()
  {
    var da=$('#IdentificatorEdit').val();
    this.adminservice.getUserInfo(da).then(dataR => {
        
      var user=dataR.user[0];
      if(user?._id)
      {    
        var d = new Date(user.birthdate)
        $('#EditFirstname').val(user.firstname);
        $('#EditLastname').val(user.lastname);   
        $('#EditBirthdate').val(user.birthdate);
        $('#EditProgram').val(user?.prog||user?.speciality);
        $('#EditIdUser').val(user._id);
        $('#EditRoleUser').val(user.role);
        this.showConfirmModal()
      }
      else{
        window.alert('user not found')
        return
      }
    })
  }
  checkClass()
  {
    var name=$('#ClassnameEdit').val();
    this.adminservice.getClassInfo(name).then(dataClass => {     
      var Class=dataClass.u[0];

      var groups:Array<any>=[]
      if(Class?.groups)
      {   
        groups=Class.groups
        this.EditedClass=Class;
        this.showConfirmModal();
        this.class_pic=this.auth.endpoint+"/uploads/class_images/"+Class._id+".jpg";
      } 
      else{
        window.alert('class not found')
        return
      }
    })
  }
  showConfirmModal()
  {
    document.getElementById("openConfirmModalButton").click();
    setTimeout(function() {$('#confirmmodalbtn').click();}, 1000);

  }










  upload(event) {
    this.event=event;
    if(!event.target.files[0] || event.target.files[0].length == 0) {
      this.msg = 'You must select an image';
      window.alert(this.msg);
			return;
		}
		
		var mimeType = event.target.files[0].type;
		
		if (mimeType.match(/image\/*/) == null) {
      this.msg = "Only images are supported";
      window.alert(this.msg);
			return;
		}
		
		var reader = new FileReader();
		reader.readAsDataURL(event.target.files[0]);
		
		reader.onload = (_event) => {
			this.msg = "";
      this.url = reader.result; 
      //alert(this.url)
		}
    this.showModal()
  }
  uploadClick(id){
    this.idClass=id;
    document.getElementById("uploadbtn").click();
  }
  
  showModal()
  {
    document.getElementById("openModalButton").click();
  }
  onSubmitImageForm(){
    this.adminservice.upload(this.event,this.idClass);
    //console.log(this.url)
    this.class_pic=this.url;
  }









}
