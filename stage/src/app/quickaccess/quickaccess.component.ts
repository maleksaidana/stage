import { Component, OnInit } from '@angular/core';
import { faUser, faEnvelopeOpenText, faPhone,faUsers,faClipboardList, faNewspaper } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from './../shared/auth.service';
import * as $ from 'jquery'
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {InfService } from './../shared/inf.service';
import {AdminService } from './../shared/admin.service';

@Component({
  selector: 'app-quickaccess',
  templateUrl: './quickaccess.component.html',
  styleUrls: ['./quickaccess.component.scss']
})
export class QuickaccessComponent implements OnInit {

  profileIcon=faUser;
  messageIcon=faEnvelopeOpenText;
  callIcon=faPhone;
  classIcon=faUsers;
  sessionsIcon =faClipboardList;
  publishIcon=faNewspaper;


  class_pic_target=this.infoservice.authService.endpoint+"/uploads/class_images/";


  Classes:any;
  Sessions:any;


  constructor(private route: ActivatedRoute,
    public authService: AuthService,
    public router: Router,
    public infoservice: InfService,
    public adminservice: AdminService,
    public rout: Router,
    ) {this.authService =authService; }

  ngOnInit(): void {

    if(!this.authService.isAdmin)
          this.myClasses();
          else
          this.sessions();
  
  }



  sessions(){
    var sessions:any;
    this.adminservice.get_sessions().then(data =>{
      sessions=data;
       // //console.log('postcomment =', comm_res);
        if(sessions)
        {
          
       
          this.Sessions=sessions;
          this.Sessions=this.Sessions.u;
          
         // //console.log("SESSIONNNNNNS",this.Sessions);
        }
    });

  }

  myClasses(){
    var classes:any;
    this.infoservice.get_my_classes().then(data =>{
      classes=data;
       // //console.log('postcomment =', comm_res);
        if(classes)
        {
          
       
          this.Classes=classes;
          this.Classes=this.Classes.u;
          ////console.log("CLASSSSSSSSSES",this.Classes[0]);
        }
    });

  }


classes(id)
{
  this.rout.navigate(['classes/'+id]);
}

classroom(id)
{
  this.rout.navigate(['classroom/'+id]);
}

  
}
