import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { AuthService } from '../shared/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  signinFormStudent: FormGroup;
  signinFormTeacher: FormGroup;
  signinFormAdmin: FormGroup;

  AdminErreur:String;
  TeacherErreur:String;
  StudentErreur:String;

  constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    public router: Router
  ) {
    this.signinFormStudent = this.fb.group({
      da: [''],
      password: ['']
    })
    this.signinFormTeacher = this.fb.group({
      da: [''],
      password: ['']
    })
    this.signinFormAdmin = this.fb.group({
      da: [''],
      password: ['']
    })
  }

  ngOnInit(): void {
    if (this.authService.isLoggedIn) {
      this.router.navigate(['home']);  
   }
   this.AdminErreur="";
   this.TeacherErreur="";
   this.StudentErreur="";
  }
 
  loginStudent() {
    this.authService.signIn(this.signinFormStudent.value,"/loginStudent").then(data =>{
      this.StudentErreur=data.toString();
      ////console.log(data);
  });
  }
  
  loginTeacher() {
    this.authService.signIn(this.signinFormTeacher.value,"/loginTeacher").then(data =>{
      this.TeacherErreur=data.toString();
      
  });
  }
  loginAdmin() {
    this.authService.signIn(this.signinFormAdmin.value,"/loginAdmin").then(data =>{
      this.AdminErreur=data.toString();
      
  });
  }
}
 