const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const base64 = require('base64url');
const path = require('path');
const multer = require('multer');
const accessTokenSecret = 'esinbeebnise';

const cors=require('cors');
app.use(cors());
app.use(bodyParser.json());
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));
app.use(bodyParser.urlencoded({ extended: true}));



/*mongo*/
const mongoose = require('mongoose');
const connection = mongoose.connection;
const student=require('./models/student');
const teacher=require('./models/teacher');
const admin=require('./models/admin');
const post=require('./models/post');
const comment=require('./models/comment');
const like=require('./models/like');
const report=require('./models/report');
const reply=require('./models/reply');
const classe=require('./models/classe');
const session=require('./models/session');



mongoose.connect('mongodb://localhost:27017/stage',({ useUnifiedTopology:true,useNewUrlParser:true }));
connection.once('open',()=>{
//console.log("MongoDB connected");
});



const authenticateJWT = (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (authHeader) {
      const token = authHeader.split(' ')[1];

      jwt.verify(token, accessTokenSecret, (err, user) => {
          if (err) {
              return res.sendStatus(403);
          }

          req.user = user;
          next();
      });
  } else {
      res.sendStatus(401);
  }
};






/*profile image download*/
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/profile_images');
    },
    filename: (req, file, cb) => {
        //console.log(file);
        cb(null, file.originalname);
    }
});
const storageclass = multer.diskStorage({
  destination: (req, file, cb) => {
      cb(null, 'uploads/class_images');
  },
  filename: (req, file, cb) => {
      //console.log(file);
      cb(null, file.originalname);
  }
});
const fileFilter = (req, file, cb) => {
    if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}
const upload = multer({ storage: storage, fileFilter: fileFilter });
const uploadclass = multer({ storage: storageclass, fileFilter: fileFilter });
/*profile image download*/



/*file post download*/
const storage_file = multer.diskStorage({
  destination: (req, file, cb) => {
    if ( file.mimetype == "application/msword" || file.mimetype == "application/vnd.ms-excel" || file.mimetype == "application/vnd.ms-powerpoint" || file.mimetype == "text/plain" || file.mimetype == "text/html" ||file.mimetype == "application/pdf")
      cb(null, 'uploads/file_posts');
      else
      cb(null, 'uploads/images_posts');
  },
  filename: (req, file, cb) => {
      //console.log(file);
      cb(null, file.originalname);
  }
});
const fileFilter_file = (req, file, cb) => {
  if ( file.mimetype == 'image/jpeg' ||file.mimetype == 'image/png' ||  file.mimetype == "application/msword" || file.mimetype == "application/vnd.ms-excel" || file.mimetype == "application/vnd.ms-powerpoint" || file.mimetype == "text/plain" || file.mimetype == "text/html" ||file.mimetype == "application/pdf") {
      cb(null, true);
  } else {
      cb(null, false);
  }
}
const upload_file = multer({ storage: storage_file, fileFilter: fileFilter_file });
/*file post download*/



app.post('/upload',authenticateJWT,  upload.any(), (req, res, next) => {
  try {
      return res.status(201).json({
          message: 'File uploded successfully'
      });
  } catch (error) {
      console.error(error);
  }
});

app.post('/uploadClass',authenticateJWT,  uploadclass.any(), (req, res, next) => {
  try {
      return res.status(201).json({
          message: 'File uploded successfully'
      });
  } catch (error) {
      console.error(error);
  }
});
/*profile image download*/


app.post('/newStudent',(req,res)=>{

    //console.log('req.body',req.body);
    
    const s=new student(req.body);
    s.role="student";
    s.save((err,s)=>{
    
      if(err){
        return res.status(500).json(err);
      }
    
      res.status(201).json(s);
    
    
    });
    
    //res.json(req.body);
    });


    app.post('/newTeacher',(req,res)=>{

      //console.log('req.body',req.body);
      
      const t=new teacher(req.body);
      t.role="teacher";
      t.save((err,s)=>{
      
        if(err){
          return res.status(500).json(err);
        }
      
        res.status(201).json(s);
      
      
      });
      
      //res.json(req.body);
      });

      app.post('/newAdmin',(req,res)=>{

        //console.log('req.body',req.body);
        
        const a=new admin(req.body);
        a.role="admin";
        a.save((err,s)=>{
        
          if(err){
            return res.status(500).json(err);
          }
        
          res.status(201).json(s);
        
        
        });
        
        //res.json(req.body);
        });

        app.post('/newClass',(req,res)=>{

          
          
          const c=new classe(req.body.c);
          const sem=req.body.semester;
        //  //console.log(s);
          c.save((err,s)=>{
          
            if(err){
              return res.status(500).json(err);
            }
          

            session.updateOne({
              "_id": mongoose.Types.ObjectId(sem)
          },{ $push: { classes: s._id }},(err, semester) => {

              if (err) {
                
                return res.send('erreur');;
              }
              if (semester) { 
            
                  return res.json({semester});
              }
             else {
                  return res.json("err");
              }
          
            }); 


         
          
          
          
          });
          
         // res.json(s);
          }) 


          app.post('/Classes',(req,res)=>{

            classe.find((err, u) => {

              if (err) {
                
                return res.send('erreur');;
              }
              if (u) { 
            
                  return res.json({u});
              }
             else {
                   return res.send('erreur');
              }
          
            }); 

          });

            app.post('/MyClasses',authenticateJWT,(req,res)=>{

              classe.aggregate([
                { $unwind: '$groups'},
                { $unwind: '$groups.group'},
                {"$lookup": {
                              "localField": "groups.group.member",
                              "from": "User",
                              "foreignField": "_id",
                              "as": "teacher"
                            } }, 
                { $match: { 'groups.group.member': mongoose.Types.ObjectId(req.user._id) }},
                  { $group:
                         {
                            "_id": "$_id",
                        "id_group": { "$first": "$groups._id" },   
                      "name": { "$first": "$name" },  
                      "description": { "$first": "$description" },  
                         }
                  },
                {
                        $project:
                        {
                            "name":1,
                            "description":1,
                           "id_group":1
                        }
                }
                
                ],(err, u) => {
  
                if (err) {
                  
                  return res.send('erreur');;
                }
                if (u) { 
              
                    return res.json({u});
                }
               else {
                     return res.send('erreur');;
                }
            
              }); 
          
          
       
            });


            app.post('/appendClass',(req,res)=>{

              
              const { grouptoadd } = req.body;
              group=grouptoadd.group;
             // member=group[0].member;

              classe.updateOne({
                "_id": mongoose.Types.ObjectId(grouptoadd._id_class)
            },{ $push: { groups: {group} } },(err, u) => {

                if (err) {
                  
                  return res.send('erreur');;
                }
                if (u) { 
              
                  


                  classe.aggregate([ 
                    { $unwind: '$groups'},
                    {"$lookup": {
                                  "localField": "groups.group.member",
                                  "from": "User",
                                  "foreignField": "_id",
                                  "as": "groups.group.student"
                                } }, 
                    { $match: { '_id': mongoose.Types.ObjectId(grouptoadd._id_class) }},
                      { $group:
                             {
                                "_id": "$_id",
                             "groups": { "$push": "$groups" }, 
                             "name": { "$first": "$name" },  
                             "description": { "$first": "$description" },  
                             }
                      },
                    {
                            $project:
                            {
                                "name":1,
                                "description":1,
                               "user":1,
                               "groups":1,
                               "group":1
                            }
                    }
                    
                    ],(err, u) => {
    
                    if (err) {
                      
                      return res.send('erreur');;
                    }
                    if (u) { 
                  
                        return res.json({u});
                    }
                   else {
                         return res.send('erreur');
                    }
                
                  }); 






                }
               else {
                     return res.send('erreur');
                }
            
              }); 

            //  return res.json({group});;
       
            });





 
            app.post('/classGroups',(req,res)=>{

              const { name } = req.body;
              classe.aggregate([ 
                { $unwind: '$groups'},
                {"$lookup": {
                              "localField": "groups.group.member",
                              "from": "User",
                              "foreignField": "_id",
                              "as": "groups.group.student"
                            } }, 
                           
                { $match: { 'name': name }},
                {"$lookup": {
                  "localField": "_id",
                  "from": "Session",
                  "foreignField": "classes",
                  "as": "semester"
                } },
                  { $group:
                         {
                            "_id": "$_id",
                         "groups": { "$push": "$groups" }, 
                         "name": { "$first": "$name" },  
                         "description": { "$first": "$description" },  
                         "semester": { "$first": "$semester" },  
                         }
                  },
                {
                        $project:
                        {
                            "name":1,
                            "description":1,
                           "user":1,
                           "groups":1,
                           "semester":1,
                           "group":1
                        }
                }
                
                ],(err, u) => {

                if (err) {
                  
                  return res.send('erreur');;
                }
                if (u) { 
              
                    return res.json({u});
                }
               else {
                     return res.send('erreur');
                }
            
              }); 

              

       
            });



            app.post('/groups',(req,res)=>{

              const { _id } = req.body;
              classe.aggregate([ 
                { $match: { '_id': mongoose.Types.ObjectId(_id) }},
                { $unwind: '$groups'},
                {"$lookup": {
                              "localField": "groups.group.member",
                              "from": "User",
                              "foreignField": "_id",
                              "as": "groups.group.student"
                            } }, 
            
                  { $group:
                         {
                            "_id": "$_id",
                         "groups": { "$push": "$groups" }, 
                         "name": { "$first": "$name" },  
                         "description": { "$first": "$description" },  
                         
                  }
                  },
                {
                        $project:
                        {
                            "name":1,
                            "description":1,
                           "groups":1,
                           
                        }
                }
                
                ],(err, u) => {

                if (err) {
                  
                  return res.send('erreur');;
                }
                if (u) { 
              
                    return res.json({u});
                }
               else {
                     return res.send('erreur');
                }
            
              }); 

              

       
            });



            app.post('/editClass',(req,res)=>{

              const OldSemester = req.body.OldSemester;
              const cl =req.body.c;

         /*     classe.updateOne(
             {  "_id": mongoose.Types.ObjectId(c._id)},
             {$set: {
              "name": c.name,
              "description": c.description,
              "groups": c.groups,
               }},(err, u) => {

                if (err) {
                  
                  return res.send('erreur');;
                }

              
              res.json(u);

                });*/
              
               
                
        classe.deleteOne({_id:mongoose.Types.ObjectId(cl._id) },(err)=>{

            if(err){
              return res.status(500).json(err);

            }
 
            const c=new classe(cl);
           
          //  //console.log(s);
            c.save((err,s)=>{
            
              if(err){
                return res.status(500).json(err);
              }
            
  
           
              res.json(s);
             
            });      

         });
      });



            app.post('/newSemester',(req,res)=>{


              
                 
           const s=new session(req.body);
          
            s.save((err,s)=>{
          
            if(err){
              return res.status(500).json(err);
            }
          
           
            res.status(201).json(s);
          
          
          });
       
            });


            
            app.post('/sessions',(req,res)=>{

              session.find((err, u) => {

                if (err) {
                  
                  return res.send('erreur');;
                }
                if (u) { 
              
                    return res.json({u});
                }
               else {
                     return res.send('erreur');
                }
            
              });
           
             });


             app.post('/semester',(req,res)=>{

              
              const { _id } = req.body;
              session.aggregate([ 
                { $match: { '_id': mongoose.Types.ObjectId(_id) }},
                { $unwind: '$classes'},
                {"$lookup": {
                              "localField": "classes",
                              "from": "Classe",
                              "foreignField": "_id",
                              "as": "classe"
                            } }, 
               
                  { $group:
                         {
                         "_id": "$_id",
                         "classes": { "$push": "$classe" }, 
                         "name": { "$first": "$name" },  
                         "description": { "$first": "$description" },  
                         }
                  },
                {
                        $project:
                        {
                            "name":1,
                            "description":1,
                            "classes":1,
                           
                        }
                }
                
                ],(err, u) => {

                if (err) {
                  
                  return res.send('erreur');;
                }
                if (u) { 
              
                    return res.json({u});
                }
               else {
                     return res.send('erreur');
                }
            
              });
           
             });



            app.post('/groupMembers',authenticateJWT,(req,res)=>{

             const { _id_group } = req.body;
            

             
             classe.aggregate([
              { $unwind: '$groups'},
              { $unwind: '$groups.group'},
              {"$lookup": {
                            "localField": "groups.group.member",
                            "from": "User",
                            "foreignField": "_id",
                            "as": "userinfo"
                          } }, 
              { $match: { 'groups._id': mongoose.Types.ObjectId(_id_group) }},
                { $group:
                       {
                          "_id": "$_id",
                           "members": { "$push": "$userinfo" },   
                     
                       }
                },
              {
                      $project:
                      {
                         
                         "members":1
                      }
              }
              
              ],(err, u) => {

              if (err) {
                
                return res.send('erreur');;
              }
              if (u) { 
            
                  return res.json({u});
              }
             else {
                   return res.send('erreur');;
              }

              

       
            });
          });




            app.post('/reports',authenticateJWT,(req,res)=>{

              
            

              report.find((err, u) => {

                if (err) {
                  
                  return res.send('erreur');;
                }
                if (u) { 
              
                    return res.json({u});
                }
               else {
                     return res.send('erreur');
                }
            
              }); 

              

       
            });
        

        app.post('/newPost' ,upload_file.any(), authenticateJWT,(req,res)=>{

         
          
            todayNumber =Date.now();

          
          const p=new post(JSON.parse(req.body.post));
          p.datee=todayNumber;
          p._id_user=req.user._id;
          p.save((err,s)=>{
          
            if(err){
              return res.status(500).json(err);
            }
          
           
            res.status(201).json(s);
          
          });
         
          
          });


          app.post('/newComment', authenticateJWT,(req,res)=>{

            todayNumber =Date.now();
       
            //jjkh
            const c=new comment(req.body);
            c._id_user=req.user._id;
            c.datee=todayNumber;
            c.save((err,s)=>{
            
              if(err){
                return res.status(500).json(err);
              }
            
              res.status(201).json(s);
            
            
            });
            
            //res.json(req.body);
            });


            app.post('/newReport', authenticateJWT,(req,res)=>{

             
              const r=new report(req.body);
            
              r.save((err,s)=>{
              
                if(err){
                  return res.status(500).json(err);
                }
              
                res.status(201).json(s);
              
              
              });
              
              //res.json(req.body);
              });


            app.post('/newLike', authenticateJWT,(req,res)=>{

              const { _id_post } = req.body;
              
        like.findOne({ "_id_user": mongoose.Types.ObjectId(req.user._id),"_id_post": mongoose.Types.ObjectId(_id_post)}, (err, u) => {

             if (err) {
        
              
             }
            if (u) { 

              return res.json({verif:0});
              
              
            
    
                
             }
             else {
              const l=new like(req.body);
              l._id_user=req.user._id;
              
              l.save((err,s)=>{
              
                if(err){
                  return res.status(500).json(err);
                }
              
                return res.json({verify:1});
              });
             }


             
  
         }); 
   


          
              
              //res.json(req.body);
 });

          app.post('/posts' , authenticateJWT,(req,res)=>{


            const { _id,skip,classroom,type } = req.body;
            
            
          if(_id!=""){
            const u=post.aggregate([
              { "$limit": 20 },
              { "$match": { "_id_user": mongoose.Types.ObjectId(_id)} },
              { "$lookup": {
               "localField": "_id_user",
               "from": "User",
               "foreignField": "_id",
               "as": "userinfo"
             } },
             { "$unwind": "$userinfo" },
            
              { $group:
                    {
                      "_id": "$_id",
                  "content": { "$first": "$content" },   
                  "datee": { "$first": "$datee" },  
                  "file": { "$first": "$file" },  
                  "image": { "$first": "$image" },  
                  "type": { "$first": "$type" },  
                  "userinfo": { "$first": "$userinfo" },
                }
             },
             { "$sort" : { "datee" : -1 } },
             { "$skip": skip },
             { "$limit": 3 },
            { "$project": {
               "content": 1,
               "datee": 1,
              "file":1,
              "image":1,
              "type":1,
               "userinfo.firstname": 1,
               "userinfo.lastname": 1,
               "userinfo._id": 1,
               "userinfo.prog": 1,
               "userinfo.fonction": 1,
               "userinfo.speciality": 1,
             } 
            }
            ], (err, post) => {
             
              if (err) {
                
                return res.json('erreur');;
              }
              if (post) { 
          
                return res.json({post});
          
              }
             else {
                  res.send('Zabour omek');
              }
            });
          }
          else{

            if(type=="0"||type=="1"||type=="2"){
            const u=post.aggregate([
              { "$limit": 20 },
              { "$match": { "classroom": classroom, "type":type} },
              { "$lookup": {
               "localField": "_id_user",
               "from": "User",
               "foreignField": "_id",
               "as": "userinfo"
             } },
             { "$unwind": "$userinfo" },
            
              { $group:
                    {
                      "_id": "$_id",
                  "content": { "$first": "$content" },   
                  "datee": { "$first": "$datee" },  
                  "file": { "$first": "$file" },  
                  "image": { "$first": "$image" },  
                  "type": { "$first": "$type" },  
                  "userinfo": { "$first": "$userinfo" },
                }
             },
             { "$sort" : { "datee" : -1 } },
             { "$skip": skip },
             { "$limit": 3 },
            { "$project": {
               "content": 1,
               "datee": 1,
              "file":1,
              "image":1,
              "type":1,
               "userinfo.firstname": 1,
               "userinfo.lastname": 1,
               "userinfo._id": 1,
               "userinfo.prog": 1,
               "userinfo.fonction": 1,
               "userinfo.speciality": 1,
             } 
            }
            ], (err, post) => {
             
              if (err) {
                
                return res.json('erreur');;
              }
              if (post) { 
          
                return res.json({post});
          
              }
             else {
                  res.send('Zabour omek');
              }
            });
          }
          else{
            const u=post.aggregate([
              { "$limit": 20 },
              { "$match": { "classroom": classroom} },
              { "$lookup": {
               "localField": "_id_user",
               "from": "User",
               "foreignField": "_id",
               "as": "userinfo"
             } },
             { "$unwind": "$userinfo" },
            
              { $group:
                    {
                      "_id": "$_id",
                  "content": { "$first": "$content" },   
                  "datee": { "$first": "$datee" },  
                  "file": { "$first": "$file" },  
                  "image": { "$first": "$image" },  
                  "type": { "$first": "$type" },  
                  "userinfo": { "$first": "$userinfo" },
                }
             },
             { "$sort" : { "datee" : -1 } },
             { "$skip": skip },
             { "$limit": 3 },
            { "$project": {
               "content": 1,
               "datee": 1,
              "file":1,
              "image":1,
              "type":1,
               "userinfo.firstname": 1,
               "userinfo.lastname": 1,
               "userinfo._id": 1,
               "userinfo.prog": 1,
               "userinfo.fonction": 1,
               "userinfo.speciality": 1,
             } 
            }
            ], (err, post) => {
             
              if (err) {
                
                return res.json('erreur');;
              }
              if (post) { 
          
                return res.json({post});
          
              }
             else {
                  res.send('Zabour omek');
              }
            });
          }
          }
            });

          
            
app.post('/comments' , authenticateJWT,(req,res)=>{


  const { _id_post,skip } = req.body;
 // //console.log(skip);
    const comm=comment.aggregate([
      { "$match": { "_id_post": mongoose.Types.ObjectId(_id_post) } },  
      { "$lookup": {
          "localField": "_id_user",
                    "from": "User",
                    "foreignField": "_id",
                    "as": "userinfo"
                  } },
        { "$unwind": "$userinfo" },
        { $group:
        {
              "_id": "$_id",
              "content": { "$first": "$content" },   
              "datee": { "$first": "$datee" },  
              "firstname": { "$first": "$userinfo.firstname" },
              "lastname": { "$first": "$userinfo.lastname" },
              "_id_user": { "$first": "$userinfo._id" },
            }
        },
        { "$sort" : { "datee" : -1 } },
        { "$skip": skip },
        { "$limit": 5 },
        { 
          "$project": {
        "_id": 1,
          "content": 1,
          "datee": 1,
        "_id_user": 1,
        "firstname":	1,
        "lastname":1,}
      }
                  
            
      
    ], (err, comment) => {

    if (err) {
      
      return res.json('erreur');;
    }
    if (comment) { 

      return res.json({comment});

    }
    else {
        res.send('Zabour omek');
    }
  })
});
/************************************************************* */

app.post('/replies' , authenticateJWT,(req,res)=>{


  const { _id_comment,skip} = req.body;
    const rep=reply.aggregate([
      { "$match": { "_id_comment": mongoose.Types.ObjectId(_id_comment) } },
      { "$lookup": {
          "localField": "_id_user",
                    "from": "User",
                    "foreignField": "_id",
                    "as": "userinfo"
                 } },
       { "$unwind": "$userinfo" },
       { $group:
        {
              "_id": "$_id",
              "content": { "$first": "$content" },   
              "datee": { "$first": "$datee" },  
              "firstname": { "$first": "$userinfo.firstname" },
              "lastname": { "$first": "$userinfo.lastname" },
              "_id_user": { "$first": "$userinfo._id" },
            }
        },
        { "$sort" : { "datee" : -1 } },
        { "$skip": skip },
        { "$limit": 3 },
       { 
         "$project": {
        "_id": 1,
          "content": 1,
          "datee": 1,
        "_id_user": 1,
        "firstname":	1,
        "lastname":1,}
      }
                  
            
      
    ], (err, reply) => {

    if (err) {
      
      return res.json('erreur');;
    }
    if (reply) { 

      return res.json({reply});

    }
   else {
        res.send('Zabour omek');
    }
  });
});

app.post('/newReply', authenticateJWT,(req,res)=>{

  todayNumber =Date.now();
  
  const r=new reply(req.body);
  r._id_user=req.user._id;
  r.datee=todayNumber;
  r.save((err,s)=>{
  
    if(err){
      return res.status(500).json(err);
    }
    res.status(201).json(r);
  
  
  });
  
  //res.json(req.body);
  });



/************************************************************** */


app.post('/countPosts' , authenticateJWT,(req,res)=>{


  const { _id_user,classroom,type } = req.body;

  if(_id_user!=""){
  const countpost=post.find( { "_id_user": mongoose.Types.ObjectId(_id_user)  }).countDocuments( (err, pos) => {

    if (err) {
      
      return res.send('erreur');;
    }
    if (pos) { 
  
        return res.json({pos});
    }
   else {
        return res.json({pos:0});
    }

  }); 
}
else{

  if(type=="0"||type=="1"||type=="2"){
  const countpost=post.find({ "classroom": classroom,"type":type  }).countDocuments( (err, pos) => {
    
    if (err) {
      
      return res.send('erreur');;
    }
    if (pos) { 
  
        return res.json({pos});
    }
   else {
        return res.json({pos:0});
    }

  }); 
}
else{
  const countpost=post.find({ "classroom": classroom  }).countDocuments( (err, pos) => {
    
    if (err) {
      
      return res.send('erreur');;
    }
    if (pos) { 
  
        return res.json({pos});
    }
   else {
        return res.json({pos:0});
    }

  }); 

}

}
 

});


  app.post('/countComments' , authenticateJWT,(req,res)=>{


    const { _id_post } = req.body;
    const countcomm=comment.find( { "_id_post": mongoose.Types.ObjectId(_id_post)  }).countDocuments( (err, comm) => {

      if (err) {
        
        return res.send('erreur');;
      }
      if (comm) { 
    
          return res.json({comm});
      }
     else {
          return res.json({comm:0});
      }
  
    }); 
   
  
  });
  app.get('/countAllPosts' , authenticateJWT,(req,res)=>{


    const { _id_post } = req.body;
    const countposts=post.countDocuments( (err, count) => {

      if (err) {
        
        return res.send('erreur');;
      }
      if (count) { 
    
          return res.json({count});
      }
     else {
          return res.json({count:0});
      }
  
    }); 
   
  
  });
  app.get('/countAllUsers' , authenticateJWT,(req,res)=>{


    const { _id_post } = req.body;
    const countusers=teacher.countDocuments( (err, count) => {

      if (err) {
        
        return res.send('erreur');;
      }
      if (count) { 
    
          return res.json({count});
      }
     else {
          return res.json({count:0});
      }
    }); 
   
  
  });
  app.post('/editTeacher' , authenticateJWT,(req,res)=>{
    var user=req.body
    //console.log('req.body',req.body);
    teacher.updateOne(  {"_id":user._id} , { $set:{ "firstname":user.firstname,"lastname":user.lastname,"birthdate":user.birthdate,"fonction":user.fonction} } , (err, user) => {

      if (err) {
        
        return res.send('erreur');;
      }
      if (user) { 
 
          res.json({"rep":"1" });
      }
     else {
      return res.json({"rep":"0" });
      }

  });
  })


  app.post('/editStudent' , authenticateJWT,(req,res)=>{
    var user=req.body
    //console.log('req.body',user);
    student.updateOne(  {"_id":user._id} , { $set:{ "firstname":user.firstname,"lastname":user.lastname,"birthdate":user.birthdate,"fonction":user.fonction} } , (err, user) => {

      if (err) {
        
        return res.send('erreur');;
      }
      if (user) { 
 
          res.json({"rep":"1" });
      }
     else {
      return res.json({"rep":"0" });
      }

  });
  })
  app.post('/countReplies' , authenticateJWT,(req,res)=>{


    const { _id_comment } = req.body;
    const countreply=reply.find( { "_id_comment": mongoose.Types.ObjectId(_id_comment)  }).countDocuments( (err, rep) => {

      if (err) {
        
        return res.send('erreur');;
      }
      if (rep) { 
    
          return res.json({rep});
      }
     else {
          return res.json({rep:0});
      }
  
    }); 
   
  
  });

  app.post('/countLikes' , authenticateJWT,(req,res)=>{


    const { _id_post } = req.body;
    const countlike=like.find( { "_id_post": mongoose.Types.ObjectId(_id_post)  }).countDocuments( (err, like) => {

      if (err) {
        
        return res.send('erreur');;
      }
      if (like) { 
    
          return res.json({like});
      }
     else {
          return res.json({like:0});
      }
  
    }); 
   
  
  });



  app.post('/editUser' , authenticateJWT,(req,res)=>{

    const {email,phone,password,newpassword,confirmnewpassword } = req.body;

  
    _id=req.user._id;


    if(newpassword.trim()=="" && confirmnewpassword.trim()==""){
    student.updateOne(  {"_id":_id} , { $set:{ "email":email,"phone":phone} } , (err, user) => {

      if (err) {
        
        return res.send('erreur');;
      }
      if (user) { 
 
          res.json({"rep":"1" });
      }
     else {
      return res.json({"rep":"0" });
      }

    });
   
  }
  else{
    student.updateOne(  {"_id":_id} , { $set:{ "email":email,"phone":phone,password:newpassword} } , (err, user) => {

      if (err) {
        
        return res.send('erreur');;
      }
      if (user) { 
 
          res.json({"rep":"1"  });
      }
     else {
      return res.json({"rep":"0" });
      }

    });
  }
   
  
  });


  app.post('/verifyPassword' , authenticateJWT,(req,res)=>{


    const { password } = req.body;
    student.findOne({ "_id": mongoose.Types.ObjectId(req.user._id), "password":password}, (err, u) => {

      if (err) {
        
        return res.send('erreur');;
      }
      if (u) { 
    
          return res.json({verify:1});
      }
     else {
          return res.json({verif:0});
      }
  
    }); 
   
  
  });

  app.post('/isLiked' , authenticateJWT,(req,res)=>{


    const { _id_post } = req.body;
    const countlike=like.find( { "_id_post": mongoose.Types.ObjectId(_id_post),"_id_user": mongoose.Types.ObjectId(req.user._id)}).countDocuments( (err, liked) => {

      if (err) {
        
        return res.send('erreur');;
      }
      if (liked) { 
    
          return res.json({liked});
      }
     else {
          return res.json({liked:0});
      }
  
    }); 
   
  
  });
          


app.post('/loginStudent', (req, res) => {

    
  const { da, password } = req.body;


  const user=student.findOne( { "da": da,"password":password,"role":"student" }, (err, user) => {

    if (err) {
      
      return res.send('erreur');;
    }
    if (user) { 
   // generate an access token
   const accessToken = jwt.sign({ _id: user._id, role: "student", firstname:user.firstname, lastname:user.lastname, prog:user.prog }, accessTokenSecret, { expiresIn: '50m' });
  //    decoded = jwt.verify(accessToken,accessTokenSecret );
        res.json({accessToken });
    }
   else {
        res.send('Username or password incorrect');
    }
  });
 

});

app.post('/loginTeacher', (req, res) => {

    
  const { da, password } = req.body;


  const user=teacher.findOne( { "da": da,"password":password, "role":"teacher"}, (err, user) => {

    if (err) {
      
      return res.json('erreur');;
    }
    if (user) { 
   // generate an access token
   const accessToken = jwt.sign({ _id: user._id, role: "teacher", firstname:user.firstname, lastname:user.lastname, speciality:user.speciality }, accessTokenSecret, { expiresIn: '50m' });
  //    decoded = jwt.verify(accessToken,accessTokenSecret );
        res.json({accessToken });
    }
   else {
        res.send('Username or password incorrect');
    }
  });
 

});



app.post('/loginAdmin', (req, res) => {

    
  const { da, password } = req.body;


  const user=admin.findOne( { "da": da,"password":password,"role":"admin" }, (err, user) => {

    if (err) {
      
      return res.json('erreur');;
    }
    if (user) { 
   // generate an access token
   const accessToken = jwt.sign({ _id: user._id, role: "admin" ,firstname:user.firstname, lastname:user.lastname, fonction:user.fonction }, accessTokenSecret, { expiresIn: '50m' });
  //    decoded = jwt.verify(accessToken,accessTokenSecret );
        res.json({accessToken });
    }
   else {
        res.send('Username or password incorrect');
    }
  }); 
   

});


app.post('/infos', authenticateJWT, (req, res) => {

    
  const { _id } = req.body;


  const user=student.find( { "_id": _id }, (err, user) => {

    if (err) {
      
      return res.json('erreur');;
    }
    if (user) { 

      return res.json({user});

    }
   else {
        res.send('Zabour omek');
    }
  });
 

});
app.post('/infosDA', authenticateJWT, (req, res) => {

    
  const { da } = req.body;


  const user=student.find( { "da": da }, (err, user) => {

    if (err) {
      
      return res.json('erreur');;
    }
    if (user) { 

      return res.json({user});

    }
   else {
        res.send('Zabour omek');
    }
  });
 

});
 

app.get('/books',  (req, res) => {

  todayNumber =Date.now();
    var year=new Date().getFullYear();
    var month=new Date().getMonth()+1;
    var day=new Date().getDate();
    var hours=new Date().getHours();
    var minutes=new Date().getMinutes();
    var seconds=new Date().getSeconds();
    var today = year+"-"+month+"-"+day+" "+hours+":"+minutes+":"+seconds;
    res.send(today); 
  
});
app.get('/home/:id', (req, res) => {
  res.send(req.user);
// res.json(books);
});



app.post('/deleteLike', authenticateJWT,  (req, res) => {

  const { _id_post } = req.body;

  

  like.deleteOne({_id_user:mongoose.Types.ObjectId(req.user._id),"_id_post":mongoose.Types.ObjectId(_id_post) },(err)=>{

    if(err){
      return res.status(500).json(err);

  }
  return res.status(201).json({msg:'Like  removed'});
  
 
  
});

});

app.post('/deletePost', authenticateJWT,  (req, res) => {


  const { _id_post } = req.body;
  

  post.deleteOne({'_id':mongoose.Types.ObjectId(_id_post) },(err)=>{

    if(err){
      return res.status(500).json(err);

     }

     comment.deleteMany({'_id_post':mongoose.Types.ObjectId(_id_post) },(err)=>{

      if(err){
        return res.status(500).json(err);
  
       }
  
      });

      reply.deleteMany({'_id_post':mongoose.Types.ObjectId(_id_post) },(err)=>{

        if(err){
          return res.status(500).json(err);
    
         }
    
        });

        like.deleteMany({'_id_post':mongoose.Types.ObjectId(_id_post) },(err)=>{

          if(err){
            return res.status(500).json(err);
      
           }
      
          });

  return res.status(201).json({msg:'post  removed'});
  
 
  
});

});


app.post('/deleteComment', authenticateJWT,  (req, res) => {



  const { _id_comment } = req.body;

comment.deleteOne({'_id':mongoose.Types.ObjectId(_id_comment) },(err)=>{

    if(err){
      return res.status(500).json(err);

  }

  reply.deleteMany({'_id_comment':mongoose.Types.ObjectId(_id_comment) },(err)=>{

    if(err){
      return res.status(500).json(err);

     }

    });
  return res.status(201).json({msg:'comment  removed'});
  
 
  
});

});

app.post('/deleteReply', authenticateJWT,  (req, res) => {



  const { _id_reply } = req.body;

reply.deleteOne({'_id':mongoose.Types.ObjectId(_id_reply) },(err)=>{

  if(err){
      return res.status(500).json(err);

  }
  return res.status(201).json({msg:'reply removed'});
  
 
  
});

});


app.post('/removeUser', authenticateJWT,  (req, res) => {



  const { _id } = req.body;

student.deleteOne({'_id':mongoose.Types.ObjectId(_id) },(err)=>{

  if(err){
      return res.status(500).json(err);

  }
  return res.status(201).json({msg:'user removed'});
});

});

app.post('/search',(req, res) => {
  const search = req.body.search;
  if(search=="")
   {
    return res.json({search:""});
  }
student.find({"$or": [{
  "firstname": new RegExp(search, 'i')
}, {
  "lastname": new RegExp(search, 'i')
}, {
  "da": new RegExp(search, 'i')
},
{
  "$expr": {
    "$regexMatch": {
      "input": { "$concat": ["$firstname", " ", "$lastname"] },
      "regex": search,  
      "options": "i"
    }
  }
},
{
  "$expr": {
    "$regexMatch": {
      "input": { "$concat": ["$lastname", " ", "$firstname"] },
      "regex": search,  
      "options": "i"
    }
  }
}

]}, (err, search) => {

  if(err){
      return res.status(500).json(err);

  }
  if (search) { 
      return res.json({search:search});
     }
    else {
         res.send('');
     }
   }).limit(5); 
    
 
 });
app.listen(3000, () => {
    console.log('Authentication service started on port 3000');
});